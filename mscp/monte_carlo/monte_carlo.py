'''
Moudle that contains the Monte Carlo simulator. 
	
'''

import numpy as np

from mscp.monte_carlo.importance import IMPORTANCE_CALCULATOR

from collections import defaultdict
from itertools import chain

class Score(object):
	''' Class to record the states of the tennis match between two players.

	Args:
		p1 (str) : player name 
		p2 (str) : player name
		bo5 (bool) : True if it is a best of 5, False otherwise

	Attributes:
		p1 (str) : player name 
		p2 (str) : player name
		bo5 (bool) : True if it is a best of 5, False otherwise
		sets : dictionary of number of sets won by players
		games : dictionary of (dictionary of games won by players) by sets
		points : dictionary of number of points won by players
		tallied_points : dictionary of number of TOTAL points won by players
		serve_stats : dictionary of serve stats by players
		cur_server (str) : current server player name
		match_over (bool) : True if match is over, False otherwise
		last_point_winner (str) : player name of the winner of the last recorded point
		missed_bp_last_returning : dictionary of (whether player missed breakpoint)
	
	Properties:
		cur_returner (str) : current returner player name
		total_games (int) : total number of games played
		winner (str) : winner of the match
	'''
	def __init__(self, p1, p2, bo5):

		self.p1 = p1 
		self.p2 = p2 
		self.bo5 = bo5  

		self.cur_set = 0

		self.sets = self.init_zeros()

		self.games = dict()
		self.games[self.cur_set] = self.init_zeros()

		self.points = self.init_zeros()
		self.tallied_points = self.init_zeros()

		self.serve_stats = defaultdict(dict) # CAV: defaultdict IS necessary
		for player in [self.p1, self.p2]:
			self.serve_stats[player]["out_of"] = 0
			self.serve_stats[player]["won"] = 0

		self.cur_server = p1
		self.match_over = False
		self.last_point_winner = None
		self.points_last_game = 0
		self.missed_bp_last_returning = {
			p1 : False,
			p2 : False,
		}

		self.importance_calculator = IMPORTANCE_CALCULATOR

	def init_zeros(self):
		''' Make a dictionary of zeros for players. ''' 
		return {
			self.p1 : 0, 
			self.p2 : 0,
		}

	def reset_points(self):
		''' Reset the points for both players. '''
		self.points = self.init_zeros()

	@property
	def cur_returner(self):
		''' Returns the current returner. ''' 
		return self.p1 if self.cur_server == self.p2 else self.p2

	@property
	def is_tiebreak(self):
		''' Returns boolean whether game is a tiebreaker '''
		games_p1 = self.games[self.cur_set][self.p1]
		games_p2 = self.games[self.cur_set][self.p2]
		return (games_p1 == 6 and games_p1 == games_p2)

	@property
	def is_breakpoint(self):
		''' Return boolean whether it is breakpoint. '''
		if self.is_tiebreak:
			return False

		pt_diff = self.points[self.cur_returner] - self.points[self.cur_server]

		if pt_diff < 1:
			return False
		elif self.points[self.cur_returner] < 3:
			return False
		else:
			return True

	@property
	def is_pt_before_bp(self):
		''' Return boolean whether it is the point before break point. 

		Notes:
			Not actually used as of yet.

		''' 
		if self.is_tiebreak:
			return False

		s_pts, r_pts = self.points[self.cur_server], self.points[self.cur_returner]
		pt_diff = r_pts - s_pts

		if pt_diff == 0 and r_pts >= 2:
			return True
		elif r_pts == 2 and s_pts <= 2:
			return True
		else: 
			return False

	def calculate_importance(self):
		''' Function to calculate the importance of a point

		Args:
			score : a <mscp.monte_carlo.monte_carlo.Score> instance

		Returns:
			importance (float) : the importance of a point
		'''

		deuce = 6 if self.is_tiebreak else 3

		pts_server = self.points[self.cur_server]
		pts_returner = self.points[self.cur_returner]

		# ???: what is this in tennis?
		if (pts_server > deuce or pts_returner > deuce):
			pt_diff = pts_server - pts_returner

			if pt_diff >= 0:
				pts_server = deuce - 1 + pt_diff
				pts_returner = deuce - 1
			else:
				pts_server = deuce - 1
				pts_returner = deuce - 1  + abs(pt_diff)

		key = (pts_server, pts_returner,
				self.games[self.cur_set][self.cur_server],
				self.games[self.cur_set][self.cur_returner],
				self.sets[self.cur_server],
				self.sets[self.cur_returner],
				self.is_tiebreak,
				not self.bo5)

		importance = self.importance_calculator[key]

		return importance


	@property
	def total_games(self):
		''' Returns the total number of games. '''
		assert(self.match_over)

		all_games = list(chain.from_iterable( (game[self.p1],game[self.p2]) 
												for key, game in self.games.items()))
		total_games = len(all_games)

		return total_games

	@property
	def winner(self):
		''' Returns the winner. '''
		assert(self.match_over)
		return self.p1 if self.sets[self.p1] > self.sets[self.p2] else self.p2

	@property
	def pct_won_serve(self):
		''' Return a dictionary of win percentage by players '''
		assert(self.match_over)
		percentage_dict = dict()
		for player in [self.p1, self.p2]:
			percentage_dict[player] = float(self.serve_stats[player]['won']) / \
				float(self.serve_stats[player]['out_of'])

		return percentage_dict

	def player_wins_set(self, winner):
		''' Function to delcare a player won a set and updates attributes 

		Args:
			winner (str) : winner player name

		Returns:
			None
		'''
		self.sets[winner] += 1

		max_sets = 3 if self.bo5 else 2

		if self.sets[winner] == max_sets: # match over
			self.match_over=True 
		else: # start a new set
			self.cur_set += 1

			self.games[self.cur_set] = self.init_zeros()
			self.points_last_game = 0

			self.missed_bp_last_returning = {
				self.p1 : False,
				self.p2 : False,
			}

	def player_wins_service_game(self, winner, loser):
		''' Function to delcare a player won a service game and updates attributes 

		Args:
			winner (str) : winner player name
			loser (str) : loser player name

		Returns:
			None
		'''
		# store points played
		self.points_last_game = self.points[winner] + self.points[loser] 

		# reset game points 
		self.points = self.init_zeros()

		# update set standings
		cur_games = self.games[self.cur_set]
		cur_games[winner] += 1
		diff = cur_games[winner] - cur_games[loser]

		if (cur_games[winner] >= 6 and diff >= 2):
			self.player_wins_set(winner)

	def player_wins_tiebreak(self, winner):
		''' Function to delcare a player won a tiebreak and updates attributes 

		Args:
			winner (str) : winner player name

		Returns:
			None
		'''

		# reset game points 
		self.points = self.init_zeros()

		# update set standings
		cur_games = self.games[self.cur_set]
		cur_games[winner] += 1

		self.player_wins_set(winner)

	def __str__(self):

		cur_server = self.cur_server
		cur_returner = self.cur_returner

		string = '{0} vs. {1}: '.format(cur_server, cur_returner)

		for _, game in self.games.items():
			string += "{0}-{1} ".format(game[cur_server], game[cur_returner])

		if not self.match_over:
			string += "{0}:{1}".format(self.points[cur_server], self.points[cur_returner])

		return string

def settle_winner(points):
	''' Function to determine the winner and loser 

	Args:
		points : dictionary of points for players

	Returns:
		winner (str) : player name 
		loser (str) : player name
	'''
	assert(len(points) == 2) 

	[p1, p2] = list(points.keys())

	winner = p1 if points[p1] > points[p2] else p2
	loser = p1 if winner == p2 else p2

	return (winner, loser)

def service_game_over(points, server, returner):
	''' Function to determine if the service game is over

	Args:
		points : dictionary of points for players
		server (str) : player name
		returner (str) : player name

	Returns:
		_ (bool) : True if service game is over, False otherwise
	'''

	pt_diff = abs(points[server] - points[returner])
	if pt_diff < 2:
		return False
	elif points[server] < 4 and points[returner] < 4:
		return False
	else:
		return True

def play_service_game(score, server, returner, prob_fun_server):
	''' Function to play the service game

	Args:
		score : a Score object
		server (str) : a player name
		returner (str) : a player name
		prob_fun_server : function(score, server) that gives the probability of server winning

	Returns:
		score : an updated Score object with the service game played
	'''

	returner_had_bp = False

	while not service_game_over(score.points, server, returner):
		# increment points played on serve:
		score.serve_stats[server]["out_of"] += 1

		prob_win_server = prob_fun_server(score, server)

		sampled_prob = np.random.rand(1)

		if sampled_prob <= prob_win_server: # server won
			score.points[server] += 1
			score.tallied_points[server] += 1
			score.last_point_winner = server
			score.serve_stats[server]["won"] += 1
		else: # returner won
			score.points[returner] += 1
			score.tallied_points[returner] += 1
			score.last_point_winner = returner

		if score.is_breakpoint:
			returner_had_bp = True

	winner, loser = settle_winner(score.points)
	score.missed_bp_last_returning[returner] = returner_had_bp and winner != returner

	score.player_wins_service_game(winner, loser)

	return score


def tiebreak_over(points, server, returner):
	''' Function to determine if the tiebreak game is over

	Args:
		points : dictionary of points for players
		server (str) : player name
		returner (str) : player name

	Returns:
		_ (bool) : True if tiebreak game is over, False otherwise
	'''

	pt_diff = abs(points[server] - points[returner])
	if pt_diff < 2:
		return False
	elif points[server] < 7 and points[returner] < 7:
		return False
	else:
		return True


def play_tiebreak(score, server, returner, prob_fun_server):
	''' Function to play the tiebreak game

	Args:
		score : a Score object
		server (str) : a player name
		returner (str) : a player name
		prob_fun_server : function(score, server) that gives the probability of server winning

	Returns:
		score : an updated Score object with the service game played
	'''

	# do not carry over missed opportunities into tiebreak:
	score.missed_bp_last_returning = {score.p1: False, score.p2: False}

	while not tiebreak_over(score.points, server, returner):
		# increment points played on serve:
		score.serve_stats[server]["out_of"] += 1

		prob_win_server = prob_fun_server(score, server)

		sampled_prob = np.random.rand(1)

		if sampled_prob <= prob_win_server:
			score.points[server] += 1
			score.tallied_points[server] += 1
			score.last_point_winner = server
			score.serve_stats[server]["won"] += 1
		else:
			score.points[returner] += 1
			score.tallied_points[returner] += 1
			score.last_point_winner = returner

		# swap the reciever and server
		point_sum = score.points[server] + score.points[returner]
		if point_sum % 2 == 1:
			server, returner = returner, server
			score.cur_server = server

	winner, loser = settle_winner(score.points)

	score.player_wins_tiebreak(winner)

	return score

def play_match(score, server, returner, prob_fun_server):
	''' Function to play the whole match

	Args:
		score : a Score object
		server (str) : a player name
		returner (str) : a player name
		prob_fun_server : function(score, server) that gives the probability of server winning

	Returns:
		score : an updated Score object with the service game played
	'''
	while not score.match_over:
		if not (score.games[score.cur_set][server] == 6
				and score.games[score.cur_set][returner] == 6):
			score = play_service_game(score, server, returner, prob_fun_server)

		else:
			score = play_tiebreak(score, server, returner, prob_fun_server)

		server, returner = returner, server
		score.cur_server = server

	return score
