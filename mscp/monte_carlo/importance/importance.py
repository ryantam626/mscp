''' Module containing utilities for calculating importance of a point in a tennis match

TODO:
	# instead of using pandas.DataFrame, use numpy.array, it is easy to assign/access values that way.
'''


import itertools
import numpy as np
import pandas as pd
from scipy.stats import binom


def game_win(pt_server, pt_returner, p=0.64, tiebreak=False):
	''' Function to calculate the server winning 

	Args:
		p (float) : probability of server winning a point
		tiebreak (bool) : True if it is a tiebreak, False otherwise
		pt_server (int) : point won by server
		pt_returner (int) : point won by returner

	'''
	win = 7 if tiebreak else 4
	deuce = 6 if tiebreak else 3
	before_deuce = deuce - 1

	if pt_server == win:
		if pt_returner < deuce:
			return 1
		else:
			return p + (1.0-p) *p*p / (1.0 - 2.0*p*(1.0-p))
	elif pt_returner == win:
		if pt_server <= 2: # ???: inconsistent, maybe tennis rule?
			return 0
		else:
			return p*p*p / (1.0 - 2.0*p*(1.0-p))
	else:
		if pt_returner < deuce:
			ret_to_before_deuce = np.arange(pt_returner, deuce) # CAV: np non-inclusive range
			p_no_deuce = np.sum(np.apply_along_axis(
				func1d=lambda pt: p*binom.pmf(deuce-pt_server,pt-pt_returner+deuce-pt_server,p),
				axis=0 , arr=ret_to_before_deuce)
			)
		else:
			p_no_deuce = 0

	p_deuce = binom.pmf(deuce-pt_server,deuce-pt_server+deuce-pt_returner,p) * p*p / (1.0 - 2.0*p*(1.0-p))

	return (p_deuce + p_no_deuce)

def expand_grid(x,y):
	s, r = np.meshgrid(x,y)
	s, r = s.flatten(), r.flatten()	
	return pd.DataFrame({'pt_server':s, 'pt_returner':r})

score_range = np.arange(0,4)
POINT_IMPORTANCE_DF = expand_grid(score_range, score_range)
POINT_IMPORTANCE_DF['importance'] = POINT_IMPORTANCE_DF.apply(
	lambda row: game_win(pt_server=row['pt_server']+1,pt_returner = row['pt_returner']) -\
		game_win(pt_server=row['pt_server'], pt_returner=row['pt_returner'] + 1),
	axis=1
	)

tb_score_range = np.arange(0,7)
TB_POINT_IMPORTANCE_DF = expand_grid(tb_score_range, tb_score_range)
TB_POINT_IMPORTANCE_DF['importance'] = TB_POINT_IMPORTANCE_DF.apply(
	lambda row: game_win(pt_server=row['pt_server']+1,pt_returner = row['pt_returner'], tiebreak=True, p=0.5) -\
		game_win(pt_server=row['pt_server'], pt_returner=row['pt_returner'] + 1, tiebreak=True, p=0.5),
	axis=1
	)

# ???: why is this needed in the first place? (ie. how did the -ve entries appear)
# CAV: pandas does not allow assignment on view
TB_POINT_IMPORTANCE_DF.ix[TB_POINT_IMPORTANCE_DF['pt_returner'] == 6, 'importance'] =\
	np.array(TB_POINT_IMPORTANCE_DF.ix[TB_POINT_IMPORTANCE_DF['pt_server'] == 6, 'importance'])

# ???: how was this computed?
BO3_SET_IMPORTANCE_MAT = np.array([[0.5,0.5],
								   [0.5,1.0]])

BO5_SET_IMPORTANCE_MAT = np.array([[0.38,0.38,0.25],
								   [0.38,0.5,0.5],
								   [0.25,0.5,1.0]])

GAME_IMPORTANCE_MAT = np.array([[0.3,0.3,0.18,0.14,0.03,0.01,np.NaN],
								[0.3,0.33,0.33,0.17,0.12,0.01,np.NaN],
								[0.3,0.33,0.37,0.37,0.14,0.08,np.NaN],
								[0.14,0.32,0.37,0.42,0.42,0.09,np.NaN],
								[0.1,0.12,0.36,0.42,0.5,0.5,np.NaN],
								[0.01,0.06,0.08,0.41,0.5,0.5,0.5],
								[np.NaN,np.NaN,np.NaN,np.NaN,np.NaN,0.5,1]])

def point_importance(p1, p2, tiebreak=False):
	''' Function to calculate the overall importance

	Args:
		p1 (int) : points won by player 1
		p2 (int) : points won by player 2
		tiebreak (bool) : True if it is a tiebreaker, Flase otherwise

	Returns:
		importance (float) : the sole improtance of the point
	'''

	if tiebreak:
		win = 7
		lookup_df = TB_POINT_IMPORTANCE_DF
	else:
		win = 4
		lookup_df = POINT_IMPORTANCE_DF

	if not (((p1 == win - 1) and (p2 == win)) or ((p2 == win - 1) and (p1 == win))):
		importance =  float(lookup_df[(lookup_df['pt_server'] == p1) & (lookup_df['pt_returner'] == p2)]['importance'])
	else:
		importance =  float(lookup_df[(lookup_df['pt_server'] == p1 - 1 ) & (lookup_df['pt_returner'] == p2 - 1 )]['importance'])

	return importance


def set_importance(s1, s2, bo3=True):
	''' Function to calculate the overall importance

	Args:
		s1 (int) : set won by player 1
		s2 (int) : set won by player 2
		bo3 (bool) : True if it is a best of 3, False otherwise

	Returns:
		importance (float) : the sole improtance of the set
	'''
	if bo3:
		x = min(s1,1)
		y = min(s2,1)
		importance = BO3_SET_IMPORTANCE_MAT[x,y]
	else:
		x = min(s1,2)
		y = min(s2,2)
		importance = BO5_SET_IMPORTANCE_MAT[x,y]

	return importance

def game_importance(g1, g2):
	''' Function to calculate the overall importance

	Args:
		g1 (int) : game won by player 1
		g2 (int) : game won by player 2

	Returns:
		importance (float) : the sole improtance of the game
	'''
	x = min(g1,6)
	y = min(g2,6)
	importance = GAME_IMPORTANCE_MAT[x,y]

	return importance

def overall_importance(p1, p2, g1, g2, s1, s2, tiebreak=False, bo3=False):
	''' Function to calculate the overall importance

	Args:
		p1 (int) : points won by player 1
		p2 (int) : points won by player 2
		g1 (int) : game won by player 1
		g2 (int) : game won by player 2
		s1 (int) : set won by player 1
		s2 (int) : set won by player 2
		tiebreak (bool) : True if it is a tiebreaker, Flase otherwise
		bo3 (bool) : True if it is a best of 3, False otherwise

	Returns:
		importance (float) : overall importance of the point
	'''
	importance = point_importance(p1, p2, tiebreak) *\
		game_importance(g1, g2) * set_importance(s1, s2, bo3)

	return importance

class LazyImportanceCalculator(object):
	''' Class to calculate the importance of a state lazily and store it.

	Basically a container with only getitem implemented.
	Key is tuple of format (p1, p2, g1, g2, s1, s2, tiebreak, bo3).
	'''
	importance_dict = dict()

	def __getitem__(self, key):
		try:
			return self.importance_dict[key]
		except KeyError:
			self.importance_dict[key] = overall_importance(*key)
			return self.importance_dict[key]

IMPORTANCE_CALCULATOR = LazyImportanceCalculator()