from datetime import datetime

# ------------------------------------------------------------------------------
class Match(object):
    ''' Store infomration about a match. '''

    def __init__(self, player_a, player_b, date, bo5):

        self.player_a = player_a
        self.player_b = player_b
        self.date = date
        self.bo5 = bo5

    def __str__(self):

        return "{0} playing against {1} on {2} in {3} format.".format(
            self.player_a, self.player_b, self.date.isoformat(),
            "best-of-five" if self.bo5 else "best-of-three")

# ------------------------------------------------------------------------------
class CompletedMatch(Match):
    ''' Adds score and, if available, point-by-point information. '''
    def __init__(self, player_a, player_b, date, bo5, score,
                 point_table=None, match_info=None):
        self.score = score
        self.point_table = point_table
        self.match_info = match_info
        super(CompletedMatch, self).__init__(player_a, player_b, date, bo5)

    @property
    def match_identifier(self):
        return '{}/{}/{}'.format(self.player_a, self.player_b, self.date.strftime('%Y%m%d'))

# ------------------------------------------------------------------------------
class PartiallyCompletedMatch(Match):
    ''' Same as CompletedMatch but with incomplete point_table and no score'''

    def __init__(self, player_a, player_b, date, bo5, point_table=None,
                 match_info=None):
        self.point_table = point_table
        self.match_info = match_info
        super(PartiallyCompletedMatch, self).__init__(
            player_a, player_b, date, bo5)

# ------------------------------------------------------------------------------
# ???: what is this  
class MatchInfo(object):
    ''' Container class for storing information about a completed match. ''' 
    def __init__(self, player_a_dict, player_b_dict, match_dict):

        self.starting_price = dict()
        self.ranking = dict()
        self.surface = None
        self.tournament = None
        self.is_challenger = None
        self.date = None
        self.bo5 = None

        for cur_dict in [player_a_dict, player_b_dict]:

            self.player_a = player_a_dict['name']
            self.player_b = player_b_dict['name']

            if 'ranking' in cur_dict:
                self.ranking[cur_dict['name']] = cur_dict['ranking']

            if 'starting_price' in cur_dict:
                self.starting_price[cur_dict['name']] = \
                    cur_dict['starting_price']

        if 'surface' in match_dict:
            self.surface = match_dict['surface']

        if 'is_challenger' in match_dict.keys():
            self.is_challenger = match_dict['is_challenger']

        if 'tournament' in match_dict:
            self.tournament = match_dict['tournament']

        if 'date' in match_dict:
            self.date = match_dict['date']

        if 'best_of' in match_dict:
            self.bo5 = match_dict['best_of'] == 5

    def has_starting_prices(self):

        right_length = (len(self.starting_price) == 2)
        valid_starting_prices = not any([x in [-1, 0] for x in
                                         self.starting_price.values()])

        return right_length and valid_starting_prices


if __name__ == "__main__":

    test = CompletedMatch("Roger Federer", "Rafael Nadal",
                          datetime(2015, 1, 1),
                          None, None)

    print(test)
