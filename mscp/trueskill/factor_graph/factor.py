'''
Moudle that contains definition of Factors in a Factor Graph.

'''

from mscp.trueskill.factor_graph import Node
from mscp.trueskill.maths import Gaussian

import numpy as np

class Factor(Node):
	''' Base Factor Node that should be inherited.

	Notes:
		# Assumed all message are apporixmately or exactly Gaussian.
		# Message are init to Gaussian(pi=0, tau=0 [ie. N(0,inf)]

	Args:
		variables (list[<Varaible>]) : list of variable node that is connected to this factor

	'''
	def __init__(self, variables):
		self.variables = variables
		for variable in variables:
			variable[self] = Gaussian(pi=0, tau=0)

	def __repr__(self):
		payload = {
			'num_connections' : len(self.variables),
			'mod' : '' if len(self.variables) == 1 else 's',
			'name' : self.__class__.__name__
		}

		return '<{name} with {num_connections} connection{mod}>'.format(**payload)

	@property
	def variable(self):
		''' Shortcut for the variable node if there is only one. '''
		assert len(self.variables) == 1
		return self.variables[0]

class PriorFactor(Factor):
	''' Factor Node that corresponds to the prior to the players' skills.

	Args:
		variable : a <mscp.trueskill.factor_graph.variable.Variable> instance
				   that corresponds to player skill variable node
		potential : the potential function associated to the factor, assumed to be a Gaussian
		dynamic : the minimum deviation of players' skill
	'''

	def __init__(self, variable, potential, dynamic=0):
		super(PriorFactor, self).__init__([variable])
		self.potential = potential
		self.dynamic = dynamic

	def down(self):
		''' Function to pass the message "downward". (ie. to the skill node)

		Args:
			None

		Returns:
			_ : delta
		'''
		sigma = np.sqrt(self.potential.sigma ** 2 + self.dynamic ** 2)
		marginal = Gaussian(mu=self.potential.mu, sigma=sigma)
		return self.variable.override_marginal(factor=self, marginal=marginal)


class SkillPerformanceFactor(Factor):
	''' Factor Node that sits between skill and performance variable nodes.

	Args:
		skill_variable : a <mscp.trueskill.factor_graph.variable.Variable> instance
				   		 that corresponds to player skill variable node
		performance_variable : a <mscp.trueskill.factor_graph.variable.Variable> instance
				   		 	   that corresponds to player performance variable node
		beta (float) : the skill/chance ratio
	'''

	def __init__(self, skill_variable, performance_variable, beta):
		super(SkillPerformanceFactor, self).__init__([skill_variable, performance_variable])
		self.skill_variable = skill_variable
		self.performance_variable = performance_variable
		self.beta = beta

	def calc_a(self, message):
		''' Function to calculate the scaling a as mentioned in the TrueSkill paper

		Args:
			message : a <Gaussian> instance that corresponds to the message into the factor

		Return:
			_ : the value of a
		'''
		return 1. / (1. + (self.beta ** 2) * message.pi)

	def down(self):
		''' Function to pass the message "downward". (ie. to performance node)

		Args:
			None

		Returns:
			_ : delta
		'''
		message = self.skill_variable / self.skill_variable[self]
		a = self.calc_a(message)
		return self.performance_variable.update_marginal(self, pi=a*message.pi, tau=a*message.tau)

	def up(self):
		''' Function to pass the message "upward". (ie. to skill node)

		Args:
			None

		Returns:
			_ : delta
		'''
		message = self.performance_variable / self.performance_variable[self]
		a = self.calc_a(message)
		return self.skill_variable.update_marginal(self, pi=a*message.pi, tau=a*message.tau)

class LinCombFactor(Factor):
	''' Factor Node that linearly combines a bunch of variable to make another

	Args:
		lin_comb_variable : a <mscp.trueskill.factor_graph.variable.Variable> instance
							that corresponds to the linear combination of variables
		original_variable_list : a list of <mscp.trueskill.factor_graph.variable.Variable> instance
		coeeff_list (list[float]) : the list of coeff of the linear combination
	'''

	def __init__(self, lin_comb_variable, original_variable_list, coeff_list):
		super(LinCombFactor, self).__init__([lin_comb_variable] + original_variable_list)
		self.lin_comb_variable = lin_comb_variable
		self.original_variable_list = original_variable_list
		self.coeff_list = coeff_list

	def down(self):
		''' Function to pass the message "downward". (ie. to summed node)

		Args:
			None

		Returns:
			_ : delta
		'''
		to_variable = self.lin_comb_variable
		from_variable_list = self.original_variable_list
		message_list = [variable[self] for variable in from_variable_list]
		coeff_list = self.coeff_list

		return self.update_marginal(to_variable ,from_variable_list ,message_list , coeff_list)

	def up(self, index):
		''' Function to pass the messae "upward". (ie. to origianl variable node)

		Args:
			None

		Returns:
			_ : delta
		'''
		assert index < len(self.coeff_list)
		denom = float(self.coeff_list[index])
		transformed_coeff = []
		for ind, coeff in enumerate(self.coeff_list):
			try:
				if ind == index:
					transformed_coeff.append(1. / denom)
				else:
					transformed_coeff.append(-coeff / denom)
			except ZeroDivisionError:
				transformed_coeff.append(0.)

		to_variable = self.original_variable_list[index]
		from_variable_list = self.original_variable_list[:]
		from_variable_list[index] = self.lin_comb_variable
		message_list = [variable[self] for variable in from_variable_list]

		return self.update_marginal(to_variable ,from_variable_list ,message_list , transformed_coeff)

	def update_marginal(self, to_variable, from_variable_list, message_list, coeff_list):
		''' Helper function to update the marginal according to any linear combination of vairables.

		Args:
			to_variable : variable node that is the linear combination
			from_variable_list (list[<Variable>]) : the basis of the linear combination
			message_list (list[<Gaussian>]) : the list of message from the basis to the factor
											  assumed to be Gaussian
			coeff_list (list[float]) : the list of coefficient of the linear combination 

		Returns:
			_ : delta
		'''

		pi_inverse = 0
		mu = 0

		for from_variable, message, coeff in zip(from_variable_list, message_list, coeff_list):
			opposite_message = from_variable / message
			mu += coeff * opposite_message.mu
			if pi_inverse == np.Inf:
				continue
			try:
				# CAV: only float operations throw ZeroDivisionError but not numpy.float64
				pi_inverse += coeff ** 2 / float(opposite_message.pi)
			except ZeroDivisionError:
				pi_inverse = np.Inf

		pi = 1. / pi_inverse
		tau = pi * mu 
		return to_variable.update_marginal(self, pi=pi, tau=tau)

class TruncateFactor(Factor):
	''' Factor Node that trunacte a Gaussian distributed variable node

	Args:
		variable : a <mscp.trueskill.factor_graph.variable.Variable> instance that is being truncated
		v_func : the additive correction term for (doubly) truncated Gaussian
		w_func : the mutliplicative correction term for (doubly) truncated Gaussian
		epsilon (float) : the draw margin
	'''

	def __init__(self, variable, v_func, w_func, epsilon):
		super(TruncateFactor, self).__init__([variable])
		self.v_func = v_func
		self.w_func = w_func
		self.epsilon = epsilon

	def up(self):
		''' Function to pass the messae "upward". (ie. to truncated variable node)

		Args:
			None

		Returns:
			_ : delta
		'''
		opposite_message = self.variable / self.variable[self]

		c = opposite_message.pi
		sqrt_c = np.sqrt(c)
		
		d = opposite_message.tau

		args = (d / sqrt_c, self.epsilon * sqrt_c)
		v = self.v_func(*args)
		w = self.w_func(*args)
		
		denom = 1. - w

		pi, tau = c / denom, (d + sqrt_c * v) / denom
		return self.variable.override_marginal(self, pi=pi, tau=tau)

