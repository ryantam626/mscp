'''
Moudle that contains definition of Variable in a Factor Graph.

TODO:
	# Is state really a good term to use here?
'''

from mscp.trueskill.factor_graph import Node
from mscp.trueskill.maths import Gaussian

import numpy as np

class Variable(Node, Gaussian):
	''' Variable Node

	Attributes:
		message_dict : dictionary of message from factor to the variable
	'''
	def __init__(self):
		super(Variable, self).__init__(pi=0,tau=0)
		self.message_dict = dict()

	def cleanse(self):
		self.pi, self.tau = 0, 0
		for message in self.message_dict.values():
			message.pi, message.tau = 0, 0

	def set(self, marginal):
		''' Function to set the variable node to a specific marginal

		Args:
			marginal : the values the variable node should take [must include pi and tau attr]

		Returns:
			delta : L_inf distance to assess convergence
		'''
		delta = self.delta(marginal)
		self.pi, self.tau = marginal.pi, marginal.tau
		return delta

	def delta(self, other):
		''' Function to get the L_inf distance of the PIs and TAUs to assess convergence

		Args:
			other : another <Gaussian> instance
		'''

		pi_delta = abs(self.pi - other.pi)
		if pi_delta == np.Inf:
			return 0.

		tau_delta = abs(self.tau - other.tau)
		return max(pi_delta, tau_delta)

	def update_marginal(self, factor, pi=None, tau=None, message=None):
		''' Function to update the marginal of the variable node by a Gaussian message

		Args:
			factor : the <Factor> child class instance where the message came from
			pi (float, optional) : the pi of the Guassian message
			tau (float, optional) : the tau of the Guassian message
			message (<Gaussian>, optional) : the actual Gaussian message
		'''
		if not ((pi is None)&(tau is None)) ^ (message is None):
			raise TypeError('Either use pi+tau or message (approx. Gaussian)')

		message = message or Gaussian(pi=pi, tau=tau)
		old_message, self[factor] = self[factor], message # save the new message
		return self.set(self / old_message * message) # recover the previous state and update

	def override_marginal(self, factor, pi=None, tau=None, marginal=None):
		''' Function to override the marginal of the variable node to a Gaussian

		Args:
			factor : the <Factor> child class instance where the "message" came from
			pi (float, optional) : the pi of the Guassian marginal to take
			tau (float, optional) : the tau of the Guassian marginal to take
			marginal (<Gaussian>, optional) : the actual Gaussian marginal to take
		'''
		if not ((pi is None)&(tau is None)) ^ (marginal is None):
			raise TypeError('Either use pi+tau or marginal (apporixmately or exactly Gaussian)')

		marginal = marginal or Gaussian(pi=pi, tau=tau)
		old_message = self[factor]
		self[factor] = marginal * old_message / self # recover the message sent to get that marginal
		return self.set(marginal)


	def __getitem__(self, factor):
		return self.message_dict[factor]

	def __setitem__(self, factor, message):
		self.message_dict[factor] = message

	def __repr__(self):
		payload = {
			'name' : 'Variable',
			'variable' : super(Variable,self).__repr__(),
			'num_connections' : len(self.message_dict),
			'mod' : '' if len(self.message_dict) == 1 else 's' 
		}

		return '<{name} {variable} with {num_connections} connection{mod}>'.format(**payload)


