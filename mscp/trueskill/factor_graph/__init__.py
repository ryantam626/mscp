

class Node(object):
	pass

from .variable import Variable
from .factor import Factor, PriorFactor

__all__ = ['Factor','Variable','PriorFactor',]