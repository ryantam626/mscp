from mscp.trueskill import BETA, DYNAMIC, CONVERGENCE_CRITERION, MAX_ITER
from mscp.trueskill.factor_graph.variable import Variable
from mscp.trueskill.maths import v_win, w_win, Gaussian
from mscp.trueskill.factor_graph.factor import (PriorFactor, SkillPerformanceFactor,
	LinCombFactor, TruncateFactor)

import itertools
import abc

class TrueSkillFactorGraph(metaclass=abc.ABCMeta):
	''' Base class for a factor graph for TrueSkill. '''

	def set_beta(self, beta, index=None):
		''' Function to set the beta of skill-performance factor

		Args:
			beta : the new beta to be set to
			index (int, optional) : index of the factor to change, all are change if None
		'''
		if index is None:
			for skill_performance_factor in self.skill_performance_factor_list:
				skill_performance_factor.beta = beta
		else:
			assert index < len(self.skill_performance_factor_list)
			self.skill_performance_factor_list[index].beta = beta


	def set_dynamic(self, dynamic, index=None):
		''' Function to set the dynamic of prior factor

		Args:
			dynamic : the new dynamic to be set to
			index (int, optional) : index of the factor to change, all are change if None
		'''
		if index is None:
			for prior_factor in self.prior_factor_list:
				prior_factor.dynamic = dynamic
		else:
			assert index < len(self.prior_factor_list)
			self.prior_factor_list[index].dynamic = dynamic

	def cleanse(self):
		''' Remove everything while preserving the structure of the factor graph. '''
		everything = itertools.chain(self.skill_variable_list,
									 self.performance_variable_list,
									 [self.performance_difference_variable])
		for obj in everything:
			obj.cleanse()

	@abc.abstractmethod
	def propagate_messages(self, convergence_criterion, max_iter):
		''' Propagate the messages in the factor graph.

		Args:
			convergence_criterion (float) : one of the stopping criterion, assess perturbation
			max_iter (int) : another stopping criterion
		Returns:
			None
		'''

class H2HFactorGraph(TrueSkillFactorGraph):
	''' Provide a reuseable factor graph class.

	Args:
		beta (float) : beta
		dynamic (float) : tau

	Attributes:
		skill_variable_list : list of skill variable (winner comes first)
		performance_variable_list : list of performance variable (winner comes first)
		performance_difference_variable : performance variable (winner-loser)

		prior_factor_list : list of prior factors (winner comes first)
		skill_performance_factor_list : list of skill-performance factors (winner comes first)
		performance_difference_factor : performance-difference factor
		truncate_factor : truncate factor

	'''
	def __init__(self, beta=BETA, dynamic=DYNAMIC):
		super(H2HFactorGraph, self).__init__()

		self.skill_variable_list = [Variable() for iter in range(2)]
		self.performance_variable_list = [Variable() for iter in range(2)]
		self.performance_difference_variable = Variable()
		self.prior_factor_list = [
			PriorFactor(variable, potential=Gaussian(pi=0, tau=0), dynamic=dynamic) for variable in self.skill_variable_list
		]

		sp_list = zip(self.skill_variable_list, self.performance_variable_list)
		self.skill_performance_factor_list = [
			SkillPerformanceFactor(skill, performance, beta) for skill, performance in sp_list
		]

		self.performance_difference_factor = LinCombFactor(self.performance_difference_variable,
														   self.performance_variable_list,
														   [+1., -1.])
		self.truncate_factor = TruncateFactor(self.performance_difference_variable,
											  v_win, w_win, 0)

	def propagate_messages(self):
		''' Propagate the messages in the factor graph. '''

		exact_factors = itertools.chain(self.prior_factor_list,
										self.skill_performance_factor_list,
										[self.performance_difference_factor])

		for f in exact_factors:
			f.down()

		# truncate factor upward to performance difference variable
		# approx but no other variables are "dependent" on this
		# so no need for converegence check
		self.truncate_factor.up()

		# difference factor upward to both performance variable
		self.performance_difference_factor.up(0)
		self.performance_difference_factor.up(1)

		# performance factor upward to skill variable
		for f in self.skill_performance_factor_list:
			f.up()


	def evaluate(self):
		''' Propagate everything down to the performance difference variable node
		to get the probability of player1 winning. '''

		exact_factors = itertools.chain(self.prior_factor_list,
										self.skill_performance_factor_list,
										[self.performance_difference_factor])

		for f in exact_factors:
			f.down()

class AdjustedH2HFactorGraph(TrueSkillFactorGraph):
	''' Provide a reuseable factor graph class for the adjusted version of H2H.

	Args:
		beta (float) : beta
		dynamic (float) : tau
		adjustment_dynamic (float) : tau for the adjustment

	Attributes:
		skill_variable_list : list of skill variable (winner comes first)
		performance_variable_list : list of performance variable (winner comes first)
		performance_difference_variable : performance variable (winner-loser)
		performance_adjustment_variable_list : list of performance adjustment variable
		adjusted_performance_variable_list : list of adjusted performance


		prior_factor_list : list of prior factors (winner comes first)
		performance_adjustment_prior_factor_list : list of performance adj prior factors (winner comes first)
		skill_performance_factor_list : list of skill-performance factors (winner comes first)
		performance_difference_factor : performance-difference factor
		truncate_factor : truncate factor

	'''
	def __init__(self, beta=BETA, dynamic=DYNAMIC, adjustment_dynamic=DYNAMIC):
		super(AdjustedH2HFactorGraph, self).__init__()

		self.skill_variable_list = [Variable() for iter in range(2)]
		self.performance_variable_list = [Variable() for iter in range(2)]
		self.performance_adjustment_variable_list = [Variable() for iter in range(2)]
		self.adjusted_performance_variable_list = [Variable() for iter in range(2)]
		self.performance_difference_variable = Variable()

		self.prior_factor_list = [
			PriorFactor(variable, potential=Gaussian(pi=0, tau=0), dynamic=dynamic) for variable in self.skill_variable_list
		]
		self.performance_adjustment_prior_factor_list = [
			PriorFactor(variable, potential=Gaussian(pi=0, tau=0), dynamic=adjustment_dynamic) for variable in self.performance_adjustment_variable_list
		]

		sp_list = zip(self.skill_variable_list, self.performance_variable_list)
		self.skill_performance_factor_list = [
			SkillPerformanceFactor(skill, performance, beta) for skill, performance in sp_list
		]

		ap_list = zip(self.performance_adjustment_variable_list, self.performance_variable_list)
		tap_list = zip(self.adjusted_performance_variable_list, ap_list)


		self.performance_adjustment_factor_list = [
			LinCombFactor(t, [a, p], [+1, +1]) for t, (a, p) in tap_list # overriden in set_eta
		]

		self.performance_difference_factor = LinCombFactor(self.performance_difference_variable,
														   self.adjusted_performance_variable_list,
														   [+1., -1.])

		self.truncate_factor = TruncateFactor(self.performance_difference_variable,
											  v_win, w_win, 0)

	def set_eta(self, eta):
		''' Function to set the ratio between the adjustment and skill

		Args:
			eta : (1 : eta) for (skill : adjustment)
		'''
		for f in self.performance_adjustment_factor_list:
			f.coeff_list = [1., eta]


	def set_adj_dynamic(self, dynamic, index=None):
		''' Function to set the dynamic of adjustment prior factor

		Args:
			dynamic : the new dynamic to be set to
			index (int, optional) : index of the factor to change, all are change if None
		'''
		if index is None:
			for prior_factor in self.performance_adjustment_prior_factor_list:
				prior_factor.dynamic = dynamic
		else:
			assert index < len(self.performance_adjustment_prior_factor_list)
			self.performance_adjustment_prior_factor_list[index].dynamic = dynamic


	def propagate_messages(self):
		''' Propagate the messages in the factor graph. '''

		exact_factors = itertools.chain(self.prior_factor_list,
										self.skill_performance_factor_list,
										self.performance_adjustment_prior_factor_list,
										self.performance_adjustment_factor_list,
										[self.performance_difference_factor])

		for f in exact_factors:
			f.down()

		# truncate factor upward to performance difference variable
		# approx but no other variables are "dependent" on this
		# so no need for converegence check
		self.truncate_factor.up()

		# difference factor upward to both performance variable
		self.performance_difference_factor.up(0)
		self.performance_difference_factor.up(1)

		# performance adjustment factor upward to performance var and performance adj var
		for f in self.performance_adjustment_factor_list:
			f.up(0)
			f.up(1)

		# performance factor upward to skill variable
		for f in self.skill_performance_factor_list:
			f.up()


	def evaluate(self):
		''' Propagate everything down to the performance difference variable node
		to get the probability of player1 winning. '''

		exact_factors = itertools.chain(self.prior_factor_list,
										self.skill_performance_factor_list,
										self.performance_adjustment_prior_factor_list,
										self.performance_adjustment_factor_list,
										[self.performance_difference_factor])

		for f in exact_factors:
			f.down()

	def cleanse(self):
		''' Remove everything while preserving the structure of the factor graph. '''
		super(AdjustedH2HFactorGraph, self).cleanse()

		everything = itertools.chain(self.performance_adjustment_variable_list,
									 self.adjusted_performance_variable_list)
		for obj in everything:
			obj.cleanse()