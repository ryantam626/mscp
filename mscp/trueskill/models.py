
from mscp.trueskill.maths import Gaussian

import numpy as np
import math
import scipy
class TrueSkill(Gaussian):
	''' Object to represent a player's skill as a Gaussian distribution.

	Args:
		mu (float) : the mean of a player's TrueSkill rating
		sigma (float) : the sigma of a player's TrueSkill rating
	'''

	def __init__(self, mu, sigma):
		super(TrueSkill, self).__init__(mu=mu, sigma=sigma)

	@classmethod
	def from_variable(cls, variable, eps=None):
		''' Function to make a TrueSkill instance according to a skill variable node.

		Args:
			variable : a <Variable> instance corresponding to a skill variable node.
			eps (float, optional) : the value to trauncate
		'''

		if eps:
			old_mu = variable.mu
			old_sigma = variable.sigma

			a = -eps
			b = eps
			alpha = (a - old_mu) / old_sigma
			beta = (b - old_mu) / old_sigma

			numer = float(scipy.stats.norm.pdf(alpha) - scipy.stats.norm.pdf(beta))
			denom = float(scipy.stats.norm.cdf(beta) - scipy.stats.norm.cdf(alpha))

			correction_factor = numer / denom

			new_mu = old_mu + old_sigma * correction_factor

			numer = alpha * scipy.stats.norm.pdf(alpha) - beta * scipy.stats.norm.pdf(beta)
			var_correction_factor = numer / denom

			new_var = old_sigma * old_sigma * (1 + var_correction_factor - correction_factor ** 2)

			ts = TrueSkill(mu=new_mu,sigma=math.sqrt(new_var))

		else:
			ts = TrueSkill(0,np.Inf) # init
			ts.pi, ts.tau = variable.pi, variable.tau
		return ts

	def __repr__(self):
		return 'TrueSkill {}'.format(super(TrueSkill, self).__repr__())
