import itertools
import pandas as pd
import abc

from collections import defaultdict, Counter

class BundledStorage(object):

	def __init__(self, environment=None, make_skill=None):
		if environment:
			self.make_skill = environment.create_trueskill_rating
		else:
			self.make_skill = make_skill
		self.skill = defaultdict(self.make_skill)
		self.metric = defaultdict(list)
		self.train_counter = Counter()
		self.python_skill = None

	def from_feature_dict(self, feature_dict, winner_str='winner', loser_str='loser'):
		''' Function to get the winner and loser's ratings from an instance.

		Args:
			feature_dict : a simple dictionary with at least winner and loser as key

		Returns:
			winner_rating : the <TrueSkill> instance of the winner
			loser_rating : the <TrueSkill> instance of the loser
		'''
		winner_rating = self.skill[feature_dict[winner_str]]
		loser_rating = self.skill[feature_dict[loser_str]]
		return winner_rating, loser_rating

	def update(self, winner, winner_rating, loser, loser_rating):
		self.skill[winner] = winner_rating
		self.skill[loser] = loser_rating

		self.train_counter[winner] += 1
		self.train_counter[loser] += 1

	def store_metric(self, key, value):
		player_list = key.split('/')[0:2]

		min_played = min([self.train_counter[player] for player in player_list])
		self.metric[key].append((min_played, value))

	def reset_metric(self):
		self.metric = defaultdict(list)

	def reset_all(self):
		self.skill = defaultdict(self.make_skill)
		self.metric = defaultdict(list)
		self.train_counter = Counter()
		
	def convert_skill_for_pickle(self):
		''' Workaround for spark to transport the cython Gaussian object. '''
		self.python_skill = { k: (v.mu, v.sigma) for k,v in self.skill.items()}
		return self.python_skill

	def convert_to_cython_skill(self):
		self.skill = {k: self.make_skill(v[0], v[1])
			for k,v in self.python_skill.items()}