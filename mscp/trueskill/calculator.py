'''
Moudle that contains definition of TrueSkill Calculator.
'''

from mscp.trueskill.models import TrueSkill

import copy

class H2HTrueSkillCalculator(object):
	''' Class that calculates the TrueSkill rating using values from an environment.

	Args:
		environment (TrueSkillEnvironment) : the environment of how TrueSkill is calculated
	'''
	def __init__(self, environment):
		self.environment = environment
		self.factor_graph = copy.deepcopy(environment.factor_graph)

	def learn(self, winner, loser):
		''' Function to get the updated TrueSkill ratings in a 2-players head to head game.

		Notes:
			# Assumed factor_graph attribute is already a well-formatted <TrueSkillFactorGraph> instance

		Args:
			winner (<TrueSkill>) : the original TrueSkill rating of the winner
			loser (<TrueSkill>) : the original TrueSkill rating of the loser

		Returns:
			updated_winner (<TrueSkill>) : the updated TrueSkill rating of the winner
			updated_loser (<TrueSkill>) : the updated TrueSkill rating of the loser
		'''
		self.factor_graph.cleanse()
		self.factor_graph.prior_factor_list[0].potential = winner
		self.factor_graph.prior_factor_list[1].potential = loser
		self.factor_graph.propagate_messages()

		updated_winner = TrueSkill.from_variable(self.factor_graph.skill_variable_list[0])
		updated_loser = TrueSkill.from_variable(self.factor_graph.skill_variable_list[1])

		return updated_winner, updated_loser

	def evaluate(self, player1, player2):
		''' Function to get the probability of player1 winning the "game".

		Args:
			player1 (<TrueSkill>) : the TrueSkill rating of the player1
			player2 (<TrueSkill>) : the TrueSkill rating of the player2

		Returns:
			win_prob (float) : the probability of player1 winning
		'''
		self.factor_graph.cleanse()
		self.factor_graph.prior_factor_list[0].potential = player1
		self.factor_graph.prior_factor_list[1].potential = player2
		self.factor_graph.evaluate()
		win_prob = 1 - self.factor_graph.performance_difference_variable.cdf(0)
		return win_prob