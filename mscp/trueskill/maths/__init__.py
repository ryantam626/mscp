import pyximport; pyximport.install()
from mscp.trueskill.maths.gaussian import Gaussian
# from mscp.trueskill.maths.gaussian_python import Gaussian

import scipy.stats

# NOTES:
# - For some reasons, using cython here does not provide any performance gain.

def v_win(t, eps):
	''' Function that calculate the additive correction term for truncated Gaussian. '''

	x = t - eps
	denom = scipy.stats.norm.cdf(x)
	numer = scipy.stats.norm.pdf(x)
	return numer / denom # ???: (if denom else - x) in the other implementation

def w_win(t, eps):
	''' Function that calculate the mutliplicative correction term for truncated Gaussian. '''

	v = v_win(t, eps)
	w = v * (v + t - eps)
	if not (0 < w < 1):
		raise IndexError("is w not in (0,1) really an issue") # borrow the IndexError by debugging
	return w

def v_draw(t, eps):
	''' Function that calculate the additive correction term for doubly truncated Gaussian. '''

	x = -eps - t
	y = eps - t
	denom = scipy.stats.norm.cdf(y) - scipy.stats.norm.cdf(x)
	numer = scipy.stats.norm.pdf(x) - scipy.stats.norm.pdf(y)
	return numer / denom

def w_draw(t, eps):
	''' Function that calculate the mutliplicative correction term for doubly truncated Gaussian. '''

	v = v_draw(t, eps)
	a = eps - t
	b = eps + t
	numer = a * scipy.stats.norm.pdf(a) + b * scipy.stats.norm.pdf(b)
	denom = scipy.stats.norm.cdf(a) - scipy.stats.norm.cdf(-b)
	return (v ** 2) + (numer / denom)
