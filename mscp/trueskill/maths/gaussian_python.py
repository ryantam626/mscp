'''
Module contains the Gaussian distribution used in the TrueSkill factor graphs.

'''

import numpy as np
import scipy.stats

class Gaussian(object):
	''' Class to concisely store a Gaussian distribution used in a TrueSKill factor graph.

	Args:
		mu (float) : mean of the Gaussian
		sigma (float) : standard deviation of the Gaussian
		pi (float) : precision of the Gaussian (inverse variance)
		tau (float) : precision adjusted mean of the Gaussian (mean * precision)
	'''

	pi = 0
	tau = 0

	def __init__(self, mu=None, sigma=None, pi=None, tau=None):
		if not ((mu is None)&(sigma is None)) ^ (pi is None)&(tau is None):
			raise TypeError('Either use mu+sigma or pi+tau')

		if mu is not None:
			if sigma == 0:
				raise ValueError('Deviation cannot be 0')

			pi = sigma ** -2
			tau = pi * mu

		self.pi = pi
		self.tau = tau

	@property
	def mu(self):
		''' Return the mean of the Gaussian. '''
		try:
			# CAV: only float operations throw ZeroDivisionError but not numpy.float64
			return float(self.tau) / float(self.pi)
		except ZeroDivisionError:
			return 0

	@property
	def sigma(self):
		''' Return the standard deviation of the Gaussian. '''
		return 1.0 / np.sqrt(self.pi) if self.pi else np.Inf

	def __mul__(self, other):
		''' Define how <Gaussian> * <Gaussian> behaves. '''
		pi, tau = self.pi + other.pi, self.tau + other.tau
		return Gaussian(pi=pi, tau=tau)

	def __truediv__(self, other):
		''' Define how <Gaussian> / <Gaussian> behaves. '''
		pi, tau = self.pi - other.pi, self.tau - other.tau
		return Gaussian(pi=pi, tau=tau)

	def __eq__(self, other):
		''' Define how <Gaussian> == <Gaussian> behaves. '''
		return self.pi == other.pi and self.tau == other.tau

	def __repr__(self):
		return 'N(mu=%.3f, sigma=%.3f)' % (self.mu, self.sigma)

	def cdf(self, x):
		return scipy.stats.norm.cdf(x ,self.mu, self.sigma)
