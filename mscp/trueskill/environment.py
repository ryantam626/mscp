'''
Module that contains the dfinition of TrueSkill environement.
'''

from mscp.trueskill.models import TrueSkill
from mscp.trueskill.factor_graph.factor_graph import H2HFactorGraph, AdjustedH2HFactorGraph

import abc

class TrueSkillEnvironment(metaclass=abc.ABCMeta):
	''' Class that wraps around the conditions use to calculate TrueSkill rating.

	Args:
		initial_mu (float) : the initial mean of a player's TrueSkill rating
		initial_sigma (float) : the initial sigma of a player's TrueSkill rating
		factor_graph : an instance of any child of <TrueSkillFactorGraph>
		beta (float) : the skill/chance ratio of the game
		dynamic (float) : the minimum deviation of a player's TrueSkill rating

	Attributes:
		initial_mu (float) : the initial mean of a player's TrueSkill rating
		initial_sigma (float) : the initial sigma of a player's TrueSkill rating
		factor_graph : an instance of any child of <TrueSkillFactorGraph>
		beta (float) : the skill/chance ratio of the game
		dynamic (float) : the minimum deviation of a player's TrueSkill rating

	Notes:
		# the factor_graph arguement simply serve as a template for the factor graph!
	'''
	def __init__(self, initial_mu, initial_sigma, factor_graph, beta=None, dynamic=None):
		self.initial_mu = initial_mu
		self.initial_sigma = initial_sigma
		self.factor_graph = factor_graph
		if beta is not None:
			factor_graph.set_beta(beta)
			self.beta = beta

		if dynamic is not None:
			factor_graph.set_dynamic(dynamic)
			self.dynamic = dynamic

	def create_trueskill_rating(self, mu=None, sigma=None):
		''' Function to create a TrueSkill instance with defaults.

		Args:
			mu (float) : the mean of a player's TrueSkill rating
			sigma (float) : the sigma of a player's TrueSkill rating
		'''
		if mu is None:
			mu = self.initial_mu
		if sigma is None:
			sigma = self.initial_sigma
		return TrueSkill(mu=mu, sigma=sigma)

	@abc.abstractmethod
	def learn(self, winner, loser):
		''' Function to get the updated TrueSkill ratings in a 2-players head to head game. '''
		pass

	@abc.abstractmethod
	def evaluate(self, player1, player2):
		''' Function to get the probability of player1 winning the "game". '''
		pass

class H2HTrueSkillEnvironment(TrueSkillEnvironment):

	def __init__(self, initial_mu, initial_sigma, beta=None, dynamic=None):
		fg = H2HFactorGraph()
		super(H2HTrueSkillEnvironment, self).__init__(initial_mu, initial_sigma, fg, beta, dynamic)


	def learn(self, winner, loser):
		''' Function to get the updated TrueSkill ratings in a 2-players head to head game.

		Notes:
			# Assumed factor_graph attribute is already a well-formatted <TrueSkillFactorGraph> instance

		Args:
			winner (<TrueSkill>) : the original TrueSkill rating of the winner
			loser (<TrueSkill>) : the original TrueSkill rating of the loser

		Returns:
			updated_winner (<TrueSkill>) : the updated TrueSkill rating of the winner
			updated_loser (<TrueSkill>) : the updated TrueSkill rating of the loser
		'''
		self.factor_graph.cleanse()
		self.factor_graph.prior_factor_list[0].potential = winner
		self.factor_graph.prior_factor_list[1].potential = loser
		self.factor_graph.propagate_messages()

		updated_winner = TrueSkill.from_variable(self.factor_graph.skill_variable_list[0])
		updated_loser = TrueSkill.from_variable(self.factor_graph.skill_variable_list[1])

		return updated_winner, updated_loser

	def evaluate(self, player1, player2):
		''' Function to get the probability of player1 winning the "game".

		Args:
			player1 (<TrueSkill>) : the TrueSkill rating of the player1
			player2 (<TrueSkill>) : the TrueSkill rating of the player2

		Returns:
			win_prob (float) : the probability of player1 winning
		'''
		self.factor_graph.cleanse()
		self.factor_graph.prior_factor_list[0].potential = player1
		self.factor_graph.prior_factor_list[1].potential = player2
		self.factor_graph.evaluate()
		win_prob = 1 - self.factor_graph.performance_difference_variable.cdf(0)
		return win_prob

class AdjustedH2HTrueSkillEnvironment(TrueSkillEnvironment):

	def __init__(self, initial_mu, initial_sigma, beta=None, dynamic=None, adj_mu=None,
			adj_sigma=None, adj_dynamic=None, eps=None, eta=0.05):
		'''
		Args:
			initial_mu
			initial_sigma
			beta
			dynamic
			adj_mu
			adj_sigma
			adj_dynamic
			eps
			eta
		'''
		fg = AdjustedH2HFactorGraph()
		fg.set_adj_dynamic(adj_dynamic)
		fg.set_eta(eta)
		self.adj_mu = adj_mu
		self.adj_sigma = adj_sigma
		self.eps = eps
		self.eta = eta
		super(AdjustedH2HTrueSkillEnvironment, self).__init__(initial_mu, initial_sigma, fg, beta, dynamic)

	def create_trueskill_adjustment(self, mu=None, sigma=None):
		if mu is None:
			mu = self.adj_mu
		if sigma is None:
			sigma = self.adj_sigma
		return self.create_trueskill_rating(mu, sigma)


	def learn(self, winner, loser, winner_adj, loser_adj):
		''' Function to get the updated TrueSkill ratings in a 2-players head to head game.

		Notes:
			# Assumed factor_graph attribute is already a well-formatted <TrueSkillFactorGraph> instance

		Args:
			winner (<TrueSkill>) : the original TrueSkill rating of the winner
			loser (<TrueSkill>) : the original TrueSkill rating of the loser
			winner_adj (<TrueSkill>) : the original TrueSkill adjustment of the winner's cluster
			loser_adj (<TrueSkill>) : the original TrueSkill adjustment of the loser's cluster

		Returns:
			updated_winner (<TrueSkill>) : the updated TrueSkill rating of the winner
			updated_loser (<TrueSkill>) : the updated TrueSkill rating of the loser
			updated_winner_adj (<TrueSkill>) : the updated TrueSkill adjustment of the winner's cluster
			updated_loser_adj (<TrueSkill>) : the updated TrueSkill adjustment of the loser's cluster
		'''
		self.factor_graph.cleanse()
		self.factor_graph.prior_factor_list[0].potential = winner
		self.factor_graph.prior_factor_list[1].potential = loser
		self.factor_graph.performance_adjustment_prior_factor_list[0].potential = winner_adj
		self.factor_graph.performance_adjustment_prior_factor_list[1].potential = loser_adj

		self.factor_graph.propagate_messages()

		updated_winner = TrueSkill.from_variable(self.factor_graph.skill_variable_list[0])
		updated_loser = TrueSkill.from_variable(self.factor_graph.skill_variable_list[1])

		updated_winner_adj = TrueSkill.from_variable(self.factor_graph.performance_adjustment_variable_list[0], self.eps)
		updated_loser_adj = TrueSkill.from_variable(self.factor_graph.performance_adjustment_variable_list[1], self.eps)

		return updated_winner, updated_loser, updated_winner_adj, updated_loser_adj

	def evaluate(self, player1, player2, player1_adj, player2_adj):
		''' Function to get the probability of player1 winning the "game".

		Args:
			player1 (<TrueSkill>) : the TrueSkill rating of the player1
			player2 (<TrueSkill>) : the TrueSkill rating of the player2
			player1_adj (<TrueSkill>) : the TrueSkill adjustment of the player1
			player2_adj (<TrueSkill>) : the TrueSkill adjustment of the player2

		Returns:
			win_prob (float) : the probability of player1 winning
		'''
		self.factor_graph.cleanse()
		self.factor_graph.prior_factor_list[0].potential = player1
		self.factor_graph.prior_factor_list[1].potential = player2
		self.factor_graph.performance_adjustment_prior_factor_list[0].potential = player1_adj
		self.factor_graph.performance_adjustment_prior_factor_list[1].potential = player2_adj
		self.factor_graph.evaluate()
		win_prob = 1 - self.factor_graph.performance_difference_variable.cdf(0)
		return win_prob