'''
This module contains the models use for ... TODO 
'''

from mscp.features.feature_extractor_helpers import make_point_summary, make_extended_point_summary

# ------------------------------------------------------------------------------
class Point(object):
	''' Wrapper class which adds information about who won the point to the "Score"
	object from the Monte Carlo simulator. 
	'''

	def __init__(self, score, server_won):
		''' 
		Args:
			score : score object from Monte Carlo simulator
			server_won (bool) : True if server won, False otherwise
		'''

		self.score = score
		self.server_won = server_won

# ------------------------------------------------------------------------------
class PointTable(object):
	''' Wrapper class for a list of points with some static functions to
	extract features from the points.
	'''
	def __init__(self, point_list, source_name):
		'''
		Args:
			TODO
		'''

		self.source_name = source_name
		self.point_list = point_list

	@staticmethod
	def find_basic_point_info(point):

		cur_data = dict()
		cur_score = point.score

		cur_data['score'] = str(cur_score)
		cur_data['server'] = cur_score.cur_server
		cur_data['returner'] = cur_score.cur_returner

		return cur_data

	@staticmethod
	def find_point_outcome(point):

		cur_data = dict()
		cur_data['server_won'] = point.server_won
		return cur_data

	@staticmethod
	def find_sloan_predictors(point):

		result = dict()
		cur_score = point.score

		# identify the point summary:
		point_summary = make_point_summary(cur_score, cur_score.cur_server)

		result.update(point_summary._asdict())
		return result

	@staticmethod
	def find_cluster_predictors(point):

	    result = dict()
	    cur_score = point.score

	    # identify the point summary:
	    point_summary = make_extended_point_summary(cur_score)

	    result.update(point_summary._asdict())
	    return result

	@staticmethod
	def extract_features(extractor_list, point):
		''' Function to extract feature using a list of extractors
		Args:
			extractor_list : a list of feature extractor that acts on a point object
			point : a point object
		Returns:
			res : a dictionary of features
		'''

		res = {}
		for extractor in extractor_list:
			res.update(extractor(point))

		return res

	def advance_point(self, point):

		self.point_list.append(point)

	def most_recent_point(self):

		assert(len(self.point_list) > 0)
		return self.point_list[-1]