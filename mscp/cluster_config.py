import itertools

BINS = 10

# define the game states to be used
is_server_list = range(2)
is_bp_list = range(2)
is_pt_before_bp_list = range(2)
is_tiebreak_list = range(2)
set_up_list = range(2)
set_down_list = range(2)
win_prob_bin_list = range(BINS)

all_states = list(itertools.product(is_server_list, is_bp_list, is_pt_before_bp_list,
	is_tiebreak_list, set_up_list, set_down_list, win_prob_bin_list))

simple_states = list(itertools.product(is_server_list, win_prob_bin_list))

# def helper func
def bin_win_prob(prob):
	return int(BINS*prob)
