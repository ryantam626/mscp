'''
This module contains the extractor of feature from raw data.
'''
import abc
from mscp.point.models import PointTable

# ------------------------------------------------------------------------------
class FeatureExtractor(metaclass=abc.ABCMeta):
	''' Base Extractor that should be inherited by other extractors. 

	Args:
		feature_subset (list[str]) : list of feature to extract, if None extract all

	Attributes:
		feature_subset (list[str]) : list of feature to extract, if None extract all
		match : any instance of child class of <mscp.match.models.Match>
	'''

	def __init__(self, feature_subset=None):
		self.feature_subset = feature_subset
		self.match = None

	@abc.abstractmethod
	def extract_features(self, point, add_outcome=False):
		''' Base function to prepare extract feature, remember to call this!

		Args:
			point : a <mscp.point.models.Point> instance
			add_outcome (bool) : whether to add the target into the features

		Returns:
			feature_extractors (list[func]) : a list of extractor functions
		'''
		assert(self.match is not None)

		return [PointTable.find_basic_point_info, ]

	def set_match(self, match):
		self.match = match

# ------------------------------------------------------------------------------
class SloanFeatureExtractor(FeatureExtractor):
	''' Base Extractor that should be inherited by other extractors.

	Args:
		feature_subset (list[str]) : list of feature to extract, if None extract all

	Attributes:
		feature (list[str]) : list of feature to extract by default
		feature_subset (list[str]) : list of feature to extract, if None extract all
		match : any instance of child class of <mscp.match.models.Match>
	'''

	features = ['server', 'returner', 'importance', 'set_down', 'point_spread',
				'is_bp', 'is_pt_b_bp', 'is_tiebreak', 'missed_opp',
				'missed_opp_return', 'points_played', 'previous_win', 'set_up']

	def __init__(self, feature_subset=None):

		if feature_subset is None:
			self.feature_subset = self.features
		else:
			self.feature_subset = feature_subset

		super(SloanFeatureExtractor, self).__init__(self.feature_subset)

	def extract_features(self, point, add_outcome=False):
		''' Function to extract the sloan features

		Args:
			point : a <mscp.point.models.Point> instance
			add_outcome (bool) : whether to add the target into the features

		Returns:
			features : a dict of extracted features
		'''
		feature_extractors = super(SloanFeatureExtractor, self).\
			extract_features(point, add_outcome)

		feature_extractors.append(PointTable.find_sloan_predictors)

		if add_outcome:
			feature_extractors.append(PointTable.find_point_outcome)

		features = PointTable.extract_features(feature_extractors, point)

		features['server'] = point.score.cur_server # ???: duplicated in basic_info
		features['returner'] = point.score.cur_returner # ???: duplicated in basic_info
		features['date'] = self.match.date

		return features


# ------------------------------------------------------------------------------
class ClusterFeatureExtractor(FeatureExtractor):
	''' Extractor for the cluster features.

	Args:
		feature_subset (list[str]) : list of feature to extract, if None extract all

	Attributes:
		feature (list[str]) : list of feature to extract by default
		feature_subset (list[str]) : list of feature to extract, if None extract all
		match : any instance of child class of <mscp.match.models.Match>
	'''


	features = ['server', 'returner', 'points_played', 'is_tiebreak',
				'is_bp','is_pt_before_bp', 'server_set_up', 'server_set_down',
				'returner_set_up', 'returner_set_down', 'server_previous_win',
				'returner_previous_win', 'server_point_spread',
				'returner_point_spread']

	def __init__(self, feature_subset=None):

		if feature_subset is None:
			self.feature_subset = self.features
		else:
			self.feature_subset = feature_subset

		super(ClusterFeatureExtractor, self).__init__(self.feature_subset)

	def extract_features(self, point, add_outcome=False):
		''' Function to extract the sloan features

		Args:
			point : a <mscp.point.models.Point> instance
			add_outcome (bool) : whether to add the target into the features

		Returns:
			features : a dict of extracted features
		'''
		feature_extractors = super(ClusterFeatureExtractor, self).\
			extract_features(point, add_outcome)

		feature_extractors.append(PointTable.find_cluster_predictors)

		if add_outcome:
			feature_extractors.append(PointTable.find_point_outcome)

		features = PointTable.extract_features(feature_extractors, point)

		features['server'] = point.score.cur_server # ???: duplicated in basic_info
		features['returner'] = point.score.cur_returner # ???: duplicated in basic_info
		features['date'] = self.match.date

		return features