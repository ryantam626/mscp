from collections import namedtuple

PointSummary = namedtuple("PointSummary",
	['is_tiebreak', 
	 'is_bp', 
	 'is_pt_before_bp',
	 'set_up', 
	 'set_down', 
	 'missed_opp',
	 'missed_opp_return', 
	 'point_spread',
	 'previous_win', 
	 'points_played',
	 'importance'])

ExtendedPointSummary = namedtuple("ExtendedPointSummary",
	['is_tiebreak',
	 'is_bp',
	 'is_pt_before_bp',
	 'server_set_up',
	 'server_set_down',
	 'returner_set_up',
	 'returner_set_down',
	 'server_previous_win',
	 'returner_previous_win',
	 'server_point_spread',
	 'returner_point_spread',
	])