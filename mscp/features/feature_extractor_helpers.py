from .models import PointSummary, ExtendedPointSummary

def make_point_summary(score, server):
	''' Function to make a <mscp.features.models.PointSummary> 
	
	Notes:
		Features are based on the current server

	Args:
		score : a <mscp.monte_carlo.monte_carlo.Score> instance

	Return:
		point_summary : a <mscp.features.models.PointSummary> instance

	'''
	point_spread = score.tallied_points[score.cur_server] - \
		score.tallied_points[score.cur_returner]

	set_diff = score.sets[score.cur_server] - score.sets[score.cur_returner]

	is_tiebreak = int(score.is_tiebreak)
	is_bp = int(score.is_breakpoint)
	is_pt_before_bp = int(score.is_pt_before_bp)
	set_up = int(set_diff >= 1)
	set_down = int(set_diff <= -1)
	missed_opp = int(score.missed_bp_last_returning[score.cur_server])
	missed_opp_return = int(score.missed_bp_last_returning[score.cur_returner])
	previous_win = int(score.last_point_winner == score.cur_server)
	points_played = score.points_last_game
	importance = score.calculate_importance()

	point_summary = PointSummary(
			is_tiebreak = is_tiebreak,
			is_bp = is_bp,
			is_pt_before_bp = is_pt_before_bp,
			set_up = set_up,
			set_down = set_down,
			missed_opp = missed_opp, 
			missed_opp_return = missed_opp_return,
			point_spread = point_spread,
			previous_win = previous_win,
			points_played = points_played,
			importance = importance,
		)

	return point_summary

def make_extended_point_summary(score):
	''' Function to make a <mscp.features.models.ExtendedPointSummary>
	Args:
		score : a <mscp.monte_carlo.monte_carlo.Score> instance

	Return:
		point_summary : a <mscp.features.models.ExtendedPointSummary> instance

	'''
	server_point_spread = score.tallied_points[score.cur_server] - \
		score.tallied_points[score.cur_returner]
	returner_point_spread = -server_point_spread

	server_set_diff = score.sets[score.cur_server] - score.sets[score.cur_returner]
	returner_set_diff = -server_set_diff

	server_set_up = int(server_set_diff >= 1)
	server_set_down = int(server_set_diff <= -1)

	returner_set_up = int(returner_set_diff >= 1)
	returner_set_down = int(returner_set_diff <= -1)

	server_previous_win = int(score.last_point_winner == score.cur_server)
	returner_previous_win = int(score.last_point_winner == score.cur_returner)

	is_tiebreak = int(score.is_tiebreak)
	is_bp = int(score.is_breakpoint)
	is_pt_before_bp = int(score.is_pt_before_bp)


	point_summary = ExtendedPointSummary(
			is_tiebreak = is_tiebreak,
			is_bp = is_bp,
			is_pt_before_bp = is_pt_before_bp,
			server_set_up = server_set_up,
			server_set_down = server_set_down,
			returner_set_up = returner_set_up,
			returner_set_down = returner_set_down,
			server_previous_win = server_previous_win,
			returner_previous_win = returner_previous_win,
			server_point_spread = server_point_spread,
			returner_point_spread = returner_point_spread
		)

	return point_summary