'''
Notes:
	Tennis dict format:

		score (tuple) : tuple of (player 1 games, player 2 games)
		match_winner_won (boolean) : boolean of whether match is won by winner 
		tiebreak_score (int/None) : None if not tiebreak 

	String score format:
		TODO

'''

import re

class BadFormattingException(Exception):
	pass

class ParsedStringScore(object):
	''' Class to parse string score and extract extra information out of it.

	Args:
		string_score (str) : the string score in Sackmann's dataset(?)
		winner (str) : player name
		loser(str) : player name
	
	Attributes:
		string_score (str) : the string score in Sackmann's dataset(?)
		winner (str) : player name
		loser(str) : player name
		sets (dict) : a tennis set dictionary, see notes for format
		bo5 (bool) : True if it was a best of five, False otherwise

	Properties:
		was_long_final_set (bool) : True if it has a long final set, False otherwise

	'''
	def __init__(self, string_score, winner, loser):

		self.winner = winner
		self.loser = loser
		self.string_score = string_score

		self.sets = self.parse_string_score(string_score)
		self.bo5 = self.determine_bo5(self.sets)

	@classmethod
	def determine_bo5(cls, sets):
		''' Determine if it is a bo5 match

		Args:
			sets : list of (tennis) sets dict
		
		Reutrns:
			_ (boolean) : bo5 boolean
		'''
		assert(len(sets) > 0)

		if len(sets) == 2:
			return False
		elif len(sets) > 3:
			return True
		else:
			# We are left with the three-set case. Either the match's winner
			# won all three sets, in which case it is best of five; or he/she
			# played best of three.
			set_winners = [cur_set['match_winner_won']
						   for cur_set in sets]

			if all(set_winners):
				return True
			else:
				return False

	@classmethod
	def parse_string_score(cls, string_score):
		''' Parse the string of scores

		Args:
			string_score (str) : the string containing all the scores

		Returns:
			result : list of (tennis) sets dict

		'''
		result = list()

		# Break by spaces to find sets:
		sets = string_score.split(' ')

		for cur_set in sets:

			# Split on hyphen to find games:
			games = cur_set.split('-')

			# There should be exactly two matches:
			if len(games) != 2:
				print('Bad formatting: {0}'.format(cur_set))
				raise BadFormattingException()

			# The first is definitely the number of games won by player 1:
			p1_games = int(games[0])

			# For p2, we need to check whether it went to a tiebreak:
			tb_score = None
			was_tb = (p1_games == 7 and games[1][0] == '6') or \
				(p1_games == 6 and games[1][0] == '7')

			if was_tb:
				p2_games = 6 if p1_games == 7 else 7
				# Find the tiebreak score:
				tb_match = re.search('\((\d+)\)', cur_set)
				tb_score = int(tb_match.group(1))
			else:
				# The number of games for p2 is just the second part of the
				# split:
				p2_games = int(games[1])

			# Put things together and record:
			cur_set = {'score': (p1_games, p2_games),
					   'match_winner_won': p1_games > p2_games,
					   'tiebreak_score': tb_score}

			result.append(cur_set)

		return result

	@property
	def was_long_final_set(self):
		''' Retuns boolean whether it has a long final set. '''
		assert(len(self.sets) > 0)
		final_set = self.sets[-1]
		long_final_set = final_set['score'][0] > 7
		return long_final_set