__author__ = 'rtam'


from mscp.importer.parser import ParsedStringScore, BadFormattingException
from mscp.utils.logger import get_logger
from mscp.utils.workers import extract_state_worker

from mscp.point.models import Point, PointTable
from mscp.match.models import CompletedMatch
from mscp.features.feature_extractors import SloanFeatureExtractor

from copy import deepcopy

import pandas as pd
import numpy as np
import multiprocessing as mp

LOGGER = get_logger(__name__)

class WasLongFinalSet(Exception):
	pass

class NotEnoughDataException(Exception):
	pass

class IncongruenceException(Exception):
	pass

class SackmannImporter(object):
	''' Class to import the csv files from Sackmann's repo.

	Args:
		file_list (list[str]) : list of path of csv files to extract
		feature_extractor : an instance of any child class of <mscp.features.FeatureExtractor>
		features (list[str]) : a list of feature to be extracted
		min_played (int) : only player with this many matches are extracted
		min_played_strict (bool) : if True, an entry with one unqualified player will be discarded
		max_matches (int) : the maximum number of matches to be extracted
		discard_challengers (bool) : if True, discard challenger entries
		discard_bo5 (bool) : if True, discard best of five entries
		min_date (<datetime.datetime>) : minimum datetime
		max_date (<datetime.datetime>) : maximum datetime
		tournament_name (str) : if set, only entries with this tournament name will be extracted
		slient (bool) : if True, logger will be disabled
		section (tuple[int]) : a tuple of (part, total) indicating data to be selected
		include_base_prob (bool) : if True, scan for winner_win_prob
								   and loser_win_prob for a base line prob

	Attributes:
		df (<pandas.DataFrame>) : the aggregated data frame of csv files
		surface_dict (dict[str:int]) : dictionary of surface code by surface string

	Properties:
		default_features (list[str]) : the default list of features
	'''

	# TODO: make use of the features attribute
	# TODO: make use of the discard_challengers attribute
	
	import mscp.monte_carlo.monte_carlo as mc

	surface_dict = {'Hard': 2, 'Grass': 3, 'Clay': 1,
		'Carpet': 0, 'Unknown': 2} # ???: should unknown be 5?

	def __init__(self,
				 file_list,
				 feature_extractor=SloanFeatureExtractor(),
				 features=None,
				 min_played=None,
				 min_played_strict=True,
				 max_matches=None,
				 discard_challengers=None,
				 discard_bo5=None,
				 min_date=None,
				 max_date=None,
				 tournament_name=None,
				 slient=False,
				 section=None,
				 include_base_prob=False):

		if features:
			self.features = features
		else:
			self.features = self.default_features

		df_list = [pd.read_csv(f) for f in file_list]
		self.df = pd.concat(df_list, ignore_index=True)


		# sort by date
		self.df['date'] = pd.to_datetime(self.df['date'])
		self.df = self.df.sort_values(by='date')

		# clear duplicates
		self.df = self.df.drop_duplicates(subset=['server1', 'server2', 'date']).reset_index(drop=True)

		if max_matches:
			if section:
				print('max_matches and section args present! Take head first')
			self.df = self.df.head(max_matches)

		if section:
			part, total = section
			assert(part<=total)
			row_selector = np.array_split(np.arange(len(self.df)),total)[part-1]
			self.df = self.df.iloc[row_selector,:]

		# filter min_played
		if min_played:
			self.df = self.filter_min_played(self.df, min_played, min_played_strict)

		if min_date:
			self.df = self.df[self.df['date'] >= min_date]

		if max_date:
			self.df = self.df[self.df['date'] <= max_date]

		self.discard_challengers = discard_challengers # TODO: add match info etc
		self.discard_bo5 = discard_bo5

		self.feature_extractor = feature_extractor

		self.slient = slient
		self.include_base_prob = include_base_prob
		if slient:
			LOGGER.disabled = True

	@staticmethod
	def filter_min_played(df, min_played, strict):
		''' Filter players that doesnt play enough matches
		
		Args:
			df : pandas df
			min_played (int) : number of matches 
			strict (bool) : remove all matches that involves at least one such player if True
							remove only matches involves two such players if False  
		Returns:
			filtered_df : pandas df that has been filtered
		'''

		player_count = df['server1'].value_counts().add(df['server2'].value_counts(),
														fill_value=0) 
		to_remove_set = set()

		all_player_set = set(player_count.index.tolist())

		for player in all_player_set:
			if (player_count[player] < min_played):
				
				to_remove_set.add(player)
		if strict:
			criterion = df.apply(lambda row: not ((row['server1'] in to_remove_set) 
												or (row['server2'] in to_remove_set)), axis=1)
		else:
			criterion = df.apply(lambda row: not ((row['server1'] in to_remove_set) 
												and (row['server2'] in to_remove_set)), axis=1)
		filtered_df = df[criterion]

		return filtered_df

	@property
	def default_features(self):
		# TODO
		return []

	@staticmethod
	def filter_tournament(df, tny_name):
		''' Filter by tournament name
		
		Args:
			df : pandas df
			tny_name (str) : tournament name 
		Returns:
			filtered_df : pandas df that has been filtered
		'''
		filtered_df = df[df['tny_name'] == tny_name]
		return filtered_df

	def extract_matches(self, num_process=1, total_num_process=1):
		''' Function to parse a single row of Sackmann's csv.

		Args:
			num_process (int) : the number of process extracting matches (NOT zero-based)
			total_num_process (int) : the total number of processes used to extract matches

		Yields:
			match : a <mscp.match.models.CompletedMatch> instance
		'''
		if ((num_process==1) and (total_num_process==1)):
			df = self.df
		else:
			assert(num_process>0)
			assert(num_process<=total_num_process)
			row_selector = np.array_split(np.arange(len(self.df)),total_num_process)[num_process-1]
			df = self.df.iloc[row_selector,:]

		self.error_row_list = []

		for j, (i, row) in enumerate(df.iterrows()):
			if j % 100 == 0:
				print("{0} - Importing match: {1} of {2}".\
					format(mp.current_process().name,j,df.shape[0]))

			# parse and record errors 
			try:
				match = self.parse_row(row)

				if match.bo5 and self.discard_bo5:
					LOGGER.info('Discard_bo5: {0}'.format(row))
					continue

				# TODO: discard challenger 

				yield match

			except BadFormattingException:
				LOGGER.warn('Got bad format : {0}'.format(row))
				continue
			except WasLongFinalSet:
				# skip if it is a long non tie break final set (?) 
				LOGGER.info('Skipping long final set : {0}'.format(row))
				continue
			except NotEnoughDataException:
				error_string = "Match not finished when all data consumed"
				LOGGER.warn('{0} : {1}'.format(error_string, row))
				cur_error_row = row.copy()
				cur_error_row['reason'] = error_string
				self.error_row_list.append(cur_error_row)
			except IncongruenceException:
				error_string = "Match ended before all sequence consumed"
				LOGGER.warn('{0} : {1}'.format(error_string, row))
				cur_error_row = row.copy()
				cur_error_row['reason'] = \
					"Match ended before all sequence consumed"
				self.error_row_list.append(cur_error_row)

		parse_error_df = pd.DataFrame(self.error_row_list)
		if not self.slient:
			parse_error_df.to_csv('./logs/failed_parsings.csv')

	def parse_row(self, row):
		''' Parse one of the row of Sackmann's csv data

		Args:
			row : one of the row from Sackmann's csv read by pandas

		Returns:
			match : a <mscp.match.models.CompletedMatch> instance

		Raises:
			WasLongFinalSet - if it was a long final set
			IncongruenceException - if not all points are parsed
		'''

		point_sequence = list(row['pbp'])
		points = list()

		# function to advance the simulation using points and point_sequences
		def curried_scorer(score, server):
			return self.sackmann_scorer(score, server, points, point_sequence)

		(winner, loser) = ((row['server1'], row['server2']) if
							   row['winner'] == 1 else
							   (row['server2'], row['server1']))

		# find additional parameters for simulation
		sackmann_score = ParsedStringScore(row['score'], winner, loser)
		server = row['server1']
		returner = row['server2']
		bo5 = sackmann_score.bo5

		if sackmann_score.was_long_final_set:
			raise WasLongFinalSet()

		# play the match using the pbp data deterministically:
		start_score = self.mc.Score(server, returner, bo5)
		final_score = self.mc.play_match(start_score,
										 start_score.cur_server,
										 start_score.cur_returner,
										 curried_scorer)

		# ensure they are consistent:
		self.ensure_consistent(sackmann_score, final_score)

		# make sure all points were parsed:
		if (len(point_sequence) != 0) and not (
				(len(point_sequence) == 1 and point_sequence[0] == '.')):
			LOGGER.warn('Final Score : {0} \n Remaining Sequence : {1}'.format(
				final_score, point_sequence))
			raise IncongruenceException()

		# store in a point table:
		point_table = PointTable(points, 'Sackmann')

		# find the date the match was played:
		date = row['date']

		# store as a CompletedMatch:
		match = CompletedMatch(final_score.p1,
							   final_score.p2,
							   date,
							   bo5,
							   final_score,
							   point_table=point_table,
							   match_info=None)
		if self.include_base_prob:
			match.winner_win_prob = row['winner_win_prob']
			match.loser_win_prob = row['loser_win_prob']
			match.winner = winner

		return match

	def parse_points(self, match):
		''' Function to make a point from a CompletedMatch instance.

		Args:
			match : a <mscp.match.models.CompletedMatch> instance

		Yields:
			result : a dictionary of features
		'''

		self.feature_extractor.set_match(match)
		self.target = 'server_won'

		for it, point in enumerate(match.point_table.point_list):
			point_features = self.feature_extractor.extract_features(point, add_outcome=True)
			result = self.prepare_feature_dict(point_features, match)
			result['date'] = match.date
			result['match_identifier'] = match.match_identifier
			result['point_identifier'] = it
			if self.include_base_prob:
				if point_features['server'] == match.winner:
					result['server_win_prob'] = match.winner_win_prob
					result['returner_win_prob'] = match.loser_win_prob
				else:
					result['server_win_prob'] = match.loser_win_prob
					result['returner_win_prob'] = match.winner_win_prob

			# TODO: other stuff here

			yield result

	def prepare_feature_dict(self, feature_dict, match):
		''' Function to post-process a point after extraction to be ready for training.

		Args:
			feature_dict (dict) : a dictionary of feature
			match : a <mscp.match.models.CompletedMatch> instance

		Returns:
			feature_dict (dict) : the modified dictionary of feature
		'''
		# convert to int
		non_int_columns = ['match_name',
						   'server',
						   'returner',
						   'score',
						   'date',
						   'importance',
						   'server_odds_prob', # NOTE: opening odds extractor required
						   'surface', # NOTE: opening odds extractor required
						   'log_server_rank', # NOTE: opening odds extractor required
						   'log_returner_rank', # NOTE: opening odds extractor required
						   'sum_service_holds']

		int_columns = feature_dict.keys() - non_int_columns

		for int_column in int_columns:
			feature_dict[int_column] = int(feature_dict[int_column])

		if set(['server', 'returner']) < feature_dict.keys():
			feature_dict = self.convert_names_to_ids(feature_dict, match)

		if 'surface' in feature_dict.keys():
			feature_dict['surface_num'] = self.surface_dict[feature_dict['surface']]


		(point_winner, point_loser) = ((feature_dict['server'], feature_dict['returner']) if
							   feature_dict['server_won'] == 1 else
							   (feature_dict['returner'], feature_dict['server']))
		feature_dict['point_winner'] = point_winner
		feature_dict['point_loser'] = point_loser

		return feature_dict

	# ???: is this the best place to put it?
	def convert_names_to_ids(self, feature_dict, match):
		''' Function to add more columns, using ids instead of player names.

		Args:
			feature_dict (dict) : a dictionary of feature
			match : a <mscp.match.models.CompletedMatch> instance

		Retruns:
			feature_dict (dict) : the dictionary of feature with two more keys
		'''

		feature_dict['server_num'] = int(match.player_a == feature_dict['server'])
		feature_dict['returner_num'] = int(match.player_a == feature_dict['server'])

		return feature_dict

	@staticmethod
	def ensure_consistent(sackmann_score, mc_score):
		''' Ensure the Sackamnn data is consistent throughout

		Args:
			sackmann_score : a prased string score object 
			mc_score : a score object played by mc simulator

		Returns:
			N/A

		Raises:
			IncongruenceException - if score / winner does not match
		'''    	

		winner_name = sackmann_score.winner

		if winner_name != mc_score.winner:
			LOGGER.warn('Winners do not match! {0} - {1}'.format(winner_name, mc_score.winner))
			raise IncongruenceException()

		for set_num, set_data in enumerate(sackmann_score.sets):

			sack_set = set_data['score']
			match_winner_games = sack_set[0]
			match_loser_games = sack_set[1]

			mc_match_winner_games = mc_score.games[set_num][winner_name]
			mc_match_loser_games = mc_score.games[set_num][sackmann_score.loser]

			if not (match_winner_games == mc_match_winner_games
					and match_loser_games == mc_match_loser_games):
				LOGGER.warn('Scores do not match!')
				raise IncongruenceException()

	@staticmethod
	def sackmann_scorer(score, server, points, point_sequence):
		''' Scorer that advance the simulation in a deterministic way.

		Provides a bridge between the Monte Carlo simulator and the
		point-by-point data. Since the simulator works by advancing according to
		the probability of the server winning, setting this to zero or one
		depending on the point-by-point data will advance the match to the
		desired situation.
		
		Raises: 
			NotEnoughDataException - not enough score
		'''
		if len(point_sequence) == 0:
			LOGGER.warn("Ran out of data at score: {0}".format(score))
			raise NotEnoughDataException()

		# pop the next command
		cur_result = point_sequence.pop(0)

		# skip the semi-colon if necessary:
		if cur_result in [';', '/', '.']:
			if len(point_sequence) == 0:
				LOGGER.warn("Ran out of data at score: {0}".format(score))
				raise NotEnoughDataException()
			cur_result = point_sequence.pop(0)

		if cur_result in ['S', 'A']:
			points.append(Point(deepcopy(score), True))
			return 1

		elif cur_result in ['R', 'D']:
			points.append(Point(deepcopy(score), False))
			return 0

		else:
			# at this stage, we must have a result:
			print("Parsing error!")
			print(cur_result)
			print(score)
			assert (False) # ???: why?

	# TODO: find this
	# def point2feature(self, point):
		# transformed_point = point2feature(point, features=self.features, target=self.target)
		# return transformed_point

	def get_next_state_token(self, num_process=1, total_num_process=1):
		''' Function to get the next state token.

		Args:
			num_process (int) : the number of process extracting matches (NOT zero-based)
			total_num_process (int) : the total number of processes used to extract matches

		Yields:
			point : a dictionary of features
		'''
		for match in self.extract_matches(num_process,total_num_process):
			for point in self.parse_points(match):
				# yield self.point2feature(point)
				yield point


class SimpleSackmannImporter(SackmannImporter):
	''' Class to impor the csv files from Sackmann's repo, but only the match level data.

	Args:
		file_list (list[str]) : list of path of csv files to extract
		min_played (int) : only player with this many matches are extracted
		min_played_strict (bool) : if True, an entry with one unqualified player will be discarded
		max_matches (int) : the maximum number of matches to be extracted
		discard_challengers (bool) : if True, discard challenger entries
		discard_bo5 (bool) : if True, discard best of five entries
		min_date (<datetime.datetime>) : minimum datetime
		max_date (<datetime.datetime>) : maximum datetime
		tournament_name (str) : if set, only entries with this tournament name will be extracted
		slient (bool) : if True, logger will be disabled

	Attributes:
		surface_dict (dict[str:int]) : dictionary of surface code by surface string
	'''

	def __init__(self,
				 file_list,
				 min_played=None,
				 min_played_strict=True,
				 max_matches=None,
				 discard_challengers=None,
				 discard_bo5=None,
				 min_date=None,
				 max_date=None,
				 tournament_name=None,
				 section=None,
				 slient=False):


		super(SimpleSackmannImporter,self).__init__(file_list,
													min_played=min_played,
													min_played_strict=min_played_strict,
													max_matches=max_matches,
													discard_challengers=discard_challengers,
													discard_bo5=discard_bo5,
													min_date=min_date,
													max_date=max_date,
													tournament_name=tournament_name,
													section=section,
													slient=slient)

	def get_next_state_token(self, num_process=1, total_num_process=1):
		''' Function to parse a single row of Sackmann's csv.

		Args:
			num_process (int) : the number of process extracting matches (NOT zero-based)
			total_num_process (int) : the total number of processes used to extract matches

		Yields:
			match : a simple dictionary of winner, loser, bo5
		'''
		if ((num_process==1) and (total_num_process==1)):
			df = self.df
		else:
			assert(num_process>0)
			assert(num_process<=total_num_process)
			row_selector = np.array_split(np.arange(len(self.df)),total_num_process)[num_process-1]
			df = self.df.iloc[row_selector,:]

		for j, (i, row) in enumerate(df.iterrows()):
			if j % 100 == 0:
				print("{0} - Importing match: {1} of {2}".\
					format(mp.current_process().name,j,df.shape[0]))

			match = self.parse_row(row)

			if match['bo5'] and self.discard_bo5:
				LOGGER.info('Discard_bo5: {0}'.format(row))
				continue

			yield match

	def parse_row(self, row):
		''' Parse one of the row of Sackmann's csv data

		Args:
			row : one of the row from Sackmann's csv read by pandas

		Returns:
			match : a simple dictionary of winner, loser, bo5
		'''

		(winner, loser) = ((row['server1'], row['server2']) if
							   row['winner'] == 1 else
							   (row['server2'], row['server1']))

		sackmann_score = ParsedStringScore(row['score'], winner, loser)
		bo5 = sackmann_score.bo5
		match_identifier = '{winner}/{loser}/{date}'.format(winner=winner,
															loser=loser,
															date=row['date'].strftime('%Y%m%d'))
		match = {'winner':winner, 'loser':loser,
				 'bo5':bo5, 'match_identifier':match_identifier}
		return match

class ParallelSackmannImporter(object):
	''' Wrapper for importing Sackmann's csv in parallel.

	Args:
		importer : any Sackmann importer instance
	'''

	def __init__(self, importer, pool_size=6):
		self.importer = importer
		self.pool_size = pool_size

	def extract(self):
		''' Function to import csv in parallel. '''

		process_list = []

		manager = mp.Manager()
		state_list_dict = manager.dict()

		for it in range(self.pool_size):
			process = mp.Process(target=extract_state_worker,
				args=(self.importer, it+1, self.pool_size, state_list_dict))
			process_list.append(process)
			process.start()

		for process in process_list:
			process.join()

		sorted_keys = sorted(state_list_dict.keys())

		everything = []

		for key in sorted_keys:
			everything.extend(state_list_dict[key])

		self.df = pd.DataFrame(everything)

		return self.df