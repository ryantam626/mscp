import numpy as np
from collections import defaultdict
def train_helper(instance, environment, storage, skip):
	''' Helper function to train and update the model.

	Args:
		instance (dict) : a simple dictionary of features
		environment : any <mscp.trueskill.environment.TrueSkillEnvironment> instance
		storage : <mscp.trueskill.storage.BundledStorage> instance
		skip (bool) : to skip over the non-recorded players or not

	'''
	try:
		winner, loser = storage.from_feature_dict(instance)
	except KeyError:
		if skip:
			return
		else:
			raise
	updated_winner, updated_loser = environment.learn(winner, loser)
	storage.update(instance['winner'], updated_winner, instance['loser'], updated_loser)

def test_helper(instance, environment, storage, skip):
	''' Helper function to test and update the metric storage.

	Args:
		instance (dict) : a simple dictionary of features
		environment : any <mscp.trueskill.environment.TrueSkillEnvironment> instance
		storage : <mscp.trueskill.storage.BundledStorage> instance
		skip (bool) : to skip over the non-recorded players or not

	'''
	try:
		winner, loser = storage.from_feature_dict(instance)
	except KeyError:
		if skip:
			return
		else:
			raise
	win_prob = environment.evaluate(winner, loser)
	# CAV: store as a list, so no need to augment it when combining after cv
	storage.store_metric(instance['identifier'], ((1. - win_prob) ** 2, win_prob))

def test_train_helper(instance, environment, storage, skip):
	''' Helper function to first test and store then train and update the model.

	Args:
		instance (dict) : a simple dictionary of features
		environment : any <mscp.trueskill.environment.TrueSkillEnvironment> instance
		storage : <mscp.trueskill.storage.BundledStorage> instance
		skip (bool) : to skip over the non-recorded players or not

	'''
	test_helper(instance, environment, storage, skip)
	train_helper(instance, environment, storage, skip)

def adjusted_train_helper(instance, environment, storage, adj_storage, skip):
	''' Helper function to train and update the model.

	Args:
		instance (dict) : a simple dictionary of features
		environment : any <mscp.trueskill.environment.TrueSkillEnvironment> instance
		storage : <mscp.trueskill.storage.BundledStorage> instance
		skip (bool) : to skip over the non-recorded players or not

	'''
	try:
		winner, loser = storage.from_feature_dict(instance)
		winner_adj, loser_adj = adj_storage.from_feature_dict(instance, 'winner_state', 'loser_state')
	except KeyError:
		if skip:
			return
		else:
			raise

	updated_winner, updated_loser, updated_winner_adj, updated_loser_adj =\
		environment.learn(winner, loser, winner_adj, loser_adj)
	storage.update(instance['winner'], updated_winner, instance['loser'], updated_loser)
	adj_storage.update(instance['winner_state'], updated_winner_adj,
		instance['loser_state'], updated_loser_adj)

def adjusted_test_helper(instance, environment, storage, adj_storage, skip):
	''' Helper function to test and update the metric storage.

	Args:
		instance (dict) : a simple dictionary of features
		environment : any <mscp.trueskill.environment.TrueSkillEnvironment> instance
		storage : <mscp.trueskill.storage.BundledStorage> instance
		skip (bool) : to skip over the non-recorded players or not

	'''
	try:
		winner, loser = storage.from_feature_dict(instance)
		winner_adj, loser_adj = adj_storage.from_feature_dict(instance, 'winner_state', 'loser_state')
	except KeyError:
		if skip:
			return
		else:
			raise
	win_prob = environment.evaluate(winner, loser, winner_adj, loser_adj)
	# CAV: store as a list, so no need to augment it when combining after cv
	storage.store_metric(instance['identifier'], ((1. - win_prob) ** 2, win_prob))

def adjusted_test_train_helper(instance, environment, storage, adj_storage, skip):
	''' Helper function to first test and store then train and update the model.

	Args:
		instance (dict) : a simple dictionary of features
		environment : any <mscp.trueskill.environment.TrueSkillEnvironment> instance
		storage : <mscp.trueskill.storage.BundledStorage> instance
		skip (bool) : to skip over the non-recorded players or not

	'''
	adjusted_test_helper(instance, environment, storage, adj_storage, skip)
	adjusted_train_helper(instance, environment, storage, adj_storage, skip)



def metric_aggregate_helper(storage, max_burnin):
	''' Helper function to aggregate the metric storage.

	Args:
		storage : <mscp.trueskill.storage.BundledStorage> instance
		max_burnin (int) : consider only evaluated entries that at least played this much
			from 0 onwards
	'''
	assert(max_burnin>=0)

	brier_score_list_dict = defaultdict(list)
	num_wrong_entry_dict = defaultdict(int)
	for identifier, output_list in storage.metric.items():
		for output in output_list:
			min_played, (brier_score, winner_prob) = output
			for burnin in range(max_burnin+1):
				if min_played > burnin:
					brier_score_list_dict[burnin].append(brier_score)
					if brier_score > 0.25:
						num_wrong_entry_dict[burnin] += 1
					elif brier_score == 0.25:
						num_wrong_entry_dict[burnin] += 0.5

	average = lambda t: (t[0], np.average(t[1]))
	avg_brier_score_dict = dict(map(average, brier_score_list_dict.items()))

	length = lambda t: (t[0], len(t[1]))
	num_entry_dict = dict(map(length, brier_score_list_dict.items()))

	return avg_brier_score_dict, num_entry_dict, num_wrong_entry_dict