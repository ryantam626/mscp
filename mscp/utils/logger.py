import logging
import time
import os

logging.Formatter.converter = time.gmtime

def get_logger(name, path=None, filename=None, format_string=None):
    logger = logging.getLogger(name)
    logger.setLevel(logging.DEBUG)

    if filename is None:
        filename = name

    if not filename.endswith('.log'):
        filename += '.log'

    if path is not None:
        if not os.path.exists(path):
            os.mkdir(path)
        fh = logging.FileHandler('{0}/{1}'.format(path, filename))
    else:
        if not os.path.exists('./logs/'):
            os.mkdir('./logs/')
        fh = logging.FileHandler('./logs/{0}'.format(filename))

    fh.setLevel(logging.DEBUG)
    if format_string:
        formatter = logging.Formatter(format_string)
    else:
        fmt = '%(asctime)s %(threadName)-10s %(processName)-10s [%(name)s] %(levelname)-8s %(message)s'
        formatter = logging.Formatter(fmt)
    fh.setFormatter(formatter)
    logger.addHandler(fh)
    return logger