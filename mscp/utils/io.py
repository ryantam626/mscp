import pandas as pd
import os
import pickle

def aggrd_res_group_to_df(aggrd_res_group):

	def gen():
		for k, v in aggrd_res_group.items():
			(mu, sigma, beta, dynamics) = k
			brier_dict, entry_dict, wrong_dict = v
			assert(set(brier_dict) == set(entry_dict))
			assert(set(entry_dict) == set(wrong_dict))
			for vk in brier_dict.keys():
				yield mu, sigma, beta, dynamics, vk ,brier_dict[vk], entry_dict[vk], wrong_dict[vk]

	df = pd.DataFrame(gen(), columns=['mu', 'sigma', 'beta', 'dynamics', 'burin',
		'brier_score', 'total_num_entry', 'total_num_wrong_entry'])

	return df

def save_aggrd_res_group(aggrd_res_group):
	df = aggrd_res_group_to_df(aggrd_res_group)
	name = input('Name of the file to be saved:\n')
	df.to_csv(os.path.join('./data/saved_results/', name))

def save_object(obj):
	name = input('Name of the file to be saved:\n')
	with open(os.path.join('./data/saved_results/', name),'wb') as f:
		pickle.dump(obj, f)