import multiprocessing as mp

from mscp.utils.helpers import (train_helper, test_helper, test_train_helper,
	adjusted_train_helper, adjusted_test_helper, adjusted_test_train_helper)

def extract_state_worker(sackmann_importer, num_process, total_num_process, state_list_dict):
	''' Worker function to get the list of states and add to a dictionary.

	Args:
		 sackmann_importer : any Sackmann repo importer
		 num_process : the current index of process
		 total_num_process : total number of process
		 state_list_dict : the dicationary for storing the states
	'''
	sackmann_generator = sackmann_importer.get_next_state_token(num_process,total_num_process)
	state_list_dict[num_process] = list(sackmann_generator)

def train_trueskill_worker(environment, storage, producer, skip=False):
	''' Worker function to train the trueskill model.

	Args:
		environment : any <mscp.trueskill.environment.TrueSkillEnvironment> instance
		storage : <mscp.trueskill.storage.BundleStorage> instance
		producer : <mscp.utils.producer.PassByProducer> instance
		skip (bool) : to skip over the non-recorded players or not
	'''
	for instance_group in iter(lambda: producer.get_next_instance_group(), None):

		train_bool = instance_group.context.get('train')
		if train_bool is None:
			helper = test_train_helper
		else:
			helper = train_helper if train_bool else test_helper

		for instance in iter(lambda: instance_group.popleft(), None):
			helper(instance, environment, storage, skip)

def train_adjusted_trueskill_worker(environment, storage, adj_storage, producer, skip=False):
	''' Worker function to train the trueskill model.

	Args:
		environment : any <mscp.trueskill.environment.TrueSkillEnvironment> instance
		storage : <mscp.trueskill.storage.BundleStorage> instance
		adj_storage : <mscp.trueskill.storage.BundleStorage> instance
		producer : <mscp.utils.producer.PassByProducer> instance
		skip (bool) : to skip over the non-recorded players or not
	'''
	for instance_group in iter(lambda: producer.get_next_instance_group(), None):

		train_bool = instance_group.context.get('train')
		if train_bool is None:
			helper = adjusted_test_train_helper
		else:
			helper = adjusted_train_helper if train_bool else adjusted_test_helper

		for instance in iter(lambda: instance_group.popleft(), None):
			helper(instance, environment, storage, adj_storage, skip)

