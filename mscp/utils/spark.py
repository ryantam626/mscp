def get_spark_context(appname):
	from pyspark import SparkContext
	sc = SparkContext(appName=appname)
	return sc