from functools import partial, reduce

# utils for collecting the results
sum_dict = lambda a,b: dict( [ (n, a.get(n, 0)+b.get(n, 0)) for n in set(a)|set(b) ] )
div_dict = lambda v,d: dict( [(n, float(d[n]/v)) for n in set(d)])
# function used by reduced (rfunc)
mets_rfunc = lambda a,b: tuple([sum_dict(x,y) for x,y in zip(a,b)])
rg_val_rfunc = lambda a,b: (None,max(a[1],b[1]),max(a[2],b[2]),mets_rfunc(a[3],b[3]))

def rg_val_avg(reduced_value):
	''' Function to take the average of the reduced value in res_group. '''
	_, n_folds, n_repeats, aggr_met = reduced_value
	n_folds += 1
	n_repeats += 1
	tot = n_folds*n_repeats
	return tuple(map(partial(div_dict, tot), aggr_met))

def aggr_res_group(rg):
	''' Function to aggregate the all the values of the res_group. '''
	r = dict([(k, rg_val_avg(reduce(rg_val_rfunc,v))) for k,v in rg.items()])
	return r

def chunks(l, n):
	"""Yield successive n-sized chunks from l."""
	for i in range(0, len(l), n):
		yield l[i:i + n]