from multiprocessing.managers import SyncManager, DictProxy, ListProxy
from collections import defaultdict, Counter
class MyManager(SyncManager):
    pass

MyManager.register('defaultdict', defaultdict, DictProxy)
MyManager.register('list', list, ListProxy)
MyManager.register('dict', dict, DictProxy)
MyManager.register('Counter', Counter, DictProxy)