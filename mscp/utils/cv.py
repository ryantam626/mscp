import numpy as np

def get_crossval_ind(n, n_folds, seed):
	''' Function that does the same thing as crossvalind in MATLAB.

	Args:
		n (int) : the number of all instances
		n_folds (int) : number of fold for the cross validation
		seed (int) : RNG seed
	'''

	# split equally first
	fold_sizes = (n // n_folds) * np.ones(n_folds, dtype=np.int)
	# deal with the remanining instances
	fold_sizes[:n % n_folds] += 1

	# create the empty index first
	indexs = np.empty(n,dtype=np.int)

	current = 0

	# fill in the index
	for index, fold_size in enumerate(fold_sizes):
		start, stop = current, current + fold_size
		indexs[start:stop] = index
		current = stop

	np.random.seed(seed)
	shuffled_indexs = np.random.permutation(indexs)

	return shuffled_indexs