source('~/workspace/imp/y5/project/mscp/r_scripts/load_csv.R')
source('~/workspace/imp/y5/project/mscp/r_scripts/plot_functions.R')

# match naive, min 0, grid 1
dir.create('~/workspace/imp/y5/project/mscp/ggplots/match_naive_min0_grid1/')
start_png('~/workspace/imp/y5/project/mscp/ggplots/match_naive_min0_grid1/match_naive_min0_grid1_burnin.png')
plot_burnin(match_naive_min0_grid1.csv)
dev.off()

start_png('~/workspace/imp/y5/project/mscp/ggplots/match_naive_min0_grid1/match_naive_min0_grid1_all.png')
plot_trueskill_all(match_naive_min0_grid1.csv, 20)
dev.off()

# match naive, min 20, grid 1
dir.create('~/workspace/imp/y5/project/mscp/ggplots/match_naive_min20_grid1/')
start_png('~/workspace/imp/y5/project/mscp/ggplots/match_naive_min20_grid1/match_naive_min20_grid1_burnin.png')
plot_burnin(match_naive_min20_grid1.csv)
dev.off()

start_png('~/workspace/imp/y5/project/mscp/ggplots/match_naive_min20_grid1/match_naive_min20_grid1_dy.png')
plot_trueskill_dy(match_naive_min20_grid1.csv, 20)
dev.off()

start_png('~/workspace/imp/y5/project/mscp/ggplots/match_naive_min20_grid1/match_naive_min20_grid1_be.png')
plot_trueskill_be(match_naive_min20_grid1.csv, 20)
dev.off()

start_png('~/workspace/imp/y5/project/mscp/ggplots/match_naive_min20_grid1/match_naive_min20_grid1_si.png')
plot_trueskill_si(match_naive_min20_grid1.csv, 20)
dev.off()

# match naive, min 20, grid 2
dir.create('~/workspace/imp/y5/project/mscp/ggplots/match_naive_min20_grid2/')
start_png('~/workspace/imp/y5/project/mscp/ggplots/match_naive_min20_grid2/match_naive_min20_grid2_burnin.png')
plot_burnin(match_naive_min20_grid2.csv)
dev.off()

start_png('~/workspace/imp/y5/project/mscp/ggplots/match_naive_min20_grid2/match_naive_min20_grid2_dy.png')
plot_trueskill_dy(match_naive_min20_grid2.csv, 20)
dev.off()

start_png('~/workspace/imp/y5/project/mscp/ggplots/match_naive_min20_grid2/match_naive_min20_grid2_be.png')
plot_trueskill_be(match_naive_min20_grid2.csv, 20)
dev.off()

start_png('~/workspace/imp/y5/project/mscp/ggplots/match_naive_min20_grid2/match_naive_min20_grid2_si.png')
plot_trueskill_si(match_naive_min20_grid2.csv, 20)
dev.off()


# point naive, min 20, grid 1
dir.create('~/workspace/imp/y5/project/mscp/ggplots/point_naive_min20_grid1/')
start_png('~/workspace/imp/y5/project/mscp/ggplots/point_naive_min20_grid1/point_naive_min20_grid1_burnin.png')
plot_burnin(point_naive_min20_grid1.csv)
dev.off()

start_png('~/workspace/imp/y5/project/mscp/ggplots/point_naive_min20_grid1/point_naive_min20_grid1_dy.png')
plot_trueskill_dy(point_naive_min20_grid1.csv, 20)
dev.off()

start_png('~/workspace/imp/y5/project/mscp/ggplots/point_naive_min20_grid1/point_naive_min20_grid1_be.png')
plot_trueskill_be(point_naive_min20_grid1.csv, 20)
dev.off()

start_png('~/workspace/imp/y5/project/mscp/ggplots/point_naive_min20_grid1/point_naive_min20_grid1_si.png')
plot_trueskill_si(point_naive_min20_grid1.csv, 20)
dev.off()


# point naive, min 20, grid 2
dir.create('~/workspace/imp/y5/project/mscp/ggplots/point_naive_min20_grid2/')
start_png('~/workspace/imp/y5/project/mscp/ggplots/point_naive_min20_grid2/point_naive_min20_grid2_burnin.png')
plot_burnin(point_naive_min20_grid2.csv)
dev.off()

start_png('~/workspace/imp/y5/project/mscp/ggplots/point_naive_min20_grid2/point_naive_min20_grid2_dy.png')
plot_trueskill_dy(point_naive_min20_grid2.csv, 20)
dev.off()

start_png('~/workspace/imp/y5/project/mscp/ggplots/point_naive_min20_grid2/point_naive_min20_grid2_be.png')
plot_trueskill_be(point_naive_min20_grid2.csv, 20)
dev.off()

start_png('~/workspace/imp/y5/project/mscp/ggplots/point_naive_min20_grid2/point_naive_min20_grid2_si.png')
plot_trueskill_si(point_naive_min20_grid2.csv, 20)
dev.off()

# toy naive grid 1
dir.create('~/workspace/imp/y5/project/mscp/ggplots/toy_naive_grid1/')
start_png('~/workspace/imp/y5/project/mscp/ggplots/toy_naive_grid1/toy_naive_grid1_burnin.png')
plot_burnin(toy_naive_grid1.csv)
dev.off()

start_png('~/workspace/imp/y5/project/mscp/ggplots/toy_naive_grid1/toy_naive_grid1_dy.png')
plot_trueskill_dy(toy_naive_grid1.csv, 20)
dev.off()

start_png('~/workspace/imp/y5/project/mscp/ggplots/toy_naive_grid1/toy_naive_grid1_be.png')
plot_trueskill_be(toy_naive_grid1.csv, 20)
dev.off()

start_png('~/workspace/imp/y5/project/mscp/ggplots/toy_naive_grid1/toy_naive_grid1_si.png')
plot_trueskill_si(toy_naive_grid1.csv, 20)
dev.off()


# toy naive grid 2
dir.create('~/workspace/imp/y5/project/mscp/ggplots/toy_naive_grid2/')
start_png('~/workspace/imp/y5/project/mscp/ggplots/toy_naive_grid2/toy_naive_grid2_burnin.png')
plot_burnin(toy_naive_grid2.csv)
dev.off()


start_png('~/workspace/imp/y5/project/mscp/ggplots/toy_naive_grid2/toy_naive_grid2_dy.png')
plot_trueskill_dy(toy_naive_grid2.csv, 20)
dev.off()

start_png('~/workspace/imp/y5/project/mscp/ggplots/toy_naive_grid2/toy_naive_grid2_be.png')
plot_trueskill_be(toy_naive_grid2.csv, 20)
dev.off()

start_png('~/workspace/imp/y5/project/mscp/ggplots/toy_naive_grid2/toy_naive_grid2_si.png')
plot_trueskill_si(toy_naive_grid2.csv, 20)
dev.off()

# toy adjusted grid 1
dir.create('~/workspace/imp/y5/project/mscp/ggplots/toy_adjusted_grid1/')
start_png('~/workspace/imp/y5/project/mscp/ggplots/toy_adjusted_grid1/toy_adjusted_grid1_burnin.png')
plot_burnin(toy_adjusted_grid1.csv)
dev.off()

start_png('~/workspace/imp/y5/project/mscp/ggplots/toy_adjusted_grid1/toy_adjusted_grid1_dy.png')
plot_adjusted_trueskill_dy(toy_adjusted_grid1.csv, 20)
dev.off()

start_png('~/workspace/imp/y5/project/mscp/ggplots/toy_adjusted_grid1/toy_adjusted_grid1_be.png')
plot_adjusted_trueskill_be(toy_adjusted_grid1.csv, 20)
dev.off()

start_png('~/workspace/imp/y5/project/mscp/ggplots/toy_adjusted_grid1/toy_adjusted_grid1_si.png')
plot_adjusted_trueskill_si(toy_adjusted_grid1.csv, 20)
dev.off()

# toy adjusted grid 2
dir.create('~/workspace/imp/y5/project/mscp/ggplots/toy_adjusted_grid2/')
start_png('~/workspace/imp/y5/project/mscp/ggplots/toy_adjusted_grid2/toy_adjusted_grid2_burnin.png')
plot_burnin(toy_adjusted_grid2.csv)
dev.off()

start_png('~/workspace/imp/y5/project/mscp/ggplots/toy_adjusted_grid2/toy_adjusted_grid2_dy.png')
plot_adjusted_trueskill_dy(toy_adjusted_grid2.csv, 20)
dev.off()

start_png('~/workspace/imp/y5/project/mscp/ggplots/toy_adjusted_grid2/toy_adjusted_grid2_be.png')
plot_adjusted_trueskill_be(toy_adjusted_grid2.csv, 20)
dev.off()

start_png('~/workspace/imp/y5/project/mscp/ggplots/toy_adjusted_grid2/toy_adjusted_grid2_si.png')
plot_adjusted_trueskill_si(toy_adjusted_grid2.csv, 20)
dev.off()


# toy 2 adjusted grid 1
dir.create('~/workspace/imp/y5/project/mscp/ggplots/toy2_adjusted_grid/')
start_png('~/workspace/imp/y5/project/mscp/ggplots/toy2_adjusted_grid/toy2_adjusted_grid_burnin.png')
plot_burnin(toy2_adjusted_grid.csv)
dev.off()

start_png('~/workspace/imp/y5/project/mscp/ggplots/toy2_adjusted_grid/toy2_adjusted_grid_dy.png')
plot_adjusted_trueskill_dy(toy2_adjusted_grid.csv, 20)
dev.off()

start_png('~/workspace/imp/y5/project/mscp/ggplots/toy2_adjusted_grid/toy2_adjusted_grid_be.png')
plot_adjusted_trueskill_be(toy2_adjusted_grid.csv, 20)
dev.off()

start_png('~/workspace/imp/y5/project/mscp/ggplots/toy2_adjusted_grid/toy2_adjusted_grid_si.png')
plot_adjusted_trueskill_si(toy2_adjusted_grid.csv, 20)
dev.off()



