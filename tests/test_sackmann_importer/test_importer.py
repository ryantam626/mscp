import os
import pandas as pd

from mscp.importer.sackmann_importer import SackmannImporter

DIR = os.path.dirname(os.path.abspath(__file__))

def test_importer():
	''' Test to see if SackmannImporter is working.

	Notes:
		# Just a sanity check.
	'''
	filename = os.path.join(DIR,'importer_test.csv')
	filename_list = [filename]

	sackmann_importer = SackmannImporter(filename_list, slient=True)

	states = [s for s in sackmann_importer.get_next_state_token()]

	output = pd.DataFrame(states)
