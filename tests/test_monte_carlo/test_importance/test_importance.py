import mscp.monte_carlo.importance.importance as imp
import numpy as np
from unittest import TestCase
import itertools

class TestImportance(TestCase):

	def assert_almost_equal_or_nans(self, arg1, arg2, acc):
		try:
			self.assertAlmostEqual(arg1, arg2, acc)
		except AssertionError:
			if not ((np.isnan(arg1)) and (np.isnan(arg2))):
				raise

	@classmethod
	def setup_class(cls):
		cls.point_range= np.arange(0,4)
		cls.point_range_tb = np.arange(0,7)
		cls.point_edge = 4
		cls.point_edge_tb = 7

		cls.game_range = np.arange(0,7)
		cls.game_edge = 7

		cls.set_range_bo3 = np.arange(0,2)
		cls.set_range_bo5 = np.arange(0,3)
		cls.set_edge_bo3 = 2
		cls.set_edge_bo5 = 3

	def test_overall_importance_bo3_tb(self):
		''' Test that overall importance works for all normal combos for bo3 and tiebreak.

		Notes:
			# Just a sanity check.

		'''

		args_list = itertools.product(self.point_range_tb, self.point_range_tb,
			self.game_range, self.game_range, self.set_range_bo3, self.set_range_bo3)

		for p1, p2, g1, g2, s1, s2 in args_list:
			imp.overall_importance(p1,p2,g1,g2,s1,s2,tiebreak=True,bo3=True)

	def test_overall_importance_bo3_non_tb(self):
		''' Test that overall importance works for all normal combos for bo3 and  non-tiebreak.

		Notes:
			# Just a sanity check.

		'''

		args_list = itertools.product(self.point_range, self.point_range,
			self.game_range, self.game_range, self.set_range_bo3, self.set_range_bo3)

		for p1, p2, g1, g2, s1, s2 in args_list:
			imp.overall_importance(p1,p2,g1,g2,s1,s2,tiebreak=False,bo3=True)

	def test_overall_importance_bo5_tb(self):
		''' Test that overall importance works for all normal combos for bo5 and tiebreak.

		Notes:
			# Just a sanity check.

		'''

		args_list = itertools.product(self.point_range_tb, self.point_range_tb,
			self.game_range, self.game_range, self.set_range_bo5, self.set_range_bo5)

		for p1, p2, g1, g2, s1, s2 in args_list:
			imp.overall_importance(p1,p2,g1,g2,s1,s2,tiebreak=True,bo3=False)

	def test_overall_importance_bo5_non_tb(self):
		''' Test that overall importance works for all normal combos for bo5 and  non-tiebreak.

		Notes:
			# Just a sanity check.

		'''

		args_list = itertools.product(self.point_range, self.point_range,
			self.game_range, self.game_range, self.set_range_bo5, self.set_range_bo5)

		for p1, p2, g1, g2, s1, s2 in args_list:
			imp.overall_importance(p1,p2,g1,g2,s1,s2,tiebreak=False,bo3=False)

	def test_overall_importance_bo3_non_tb_edge(self):
		''' Test that overall importance works for the edge cases for bo3 and  non-tiebreak. '''

		# point_edge1_arg_list = itertools.product([self.point_edge], self.point_range,
		# 	self.game_range, self.game_range, self.set_range_bo3, self.set_range_bo3)

		# point_edge2_arg_list = itertools.product(self.point_range, [self.point_edge],
		# 	self.game_range, self.game_range, self.set_range_bo3, self.set_range_bo3)

		game_edge1_arg_list = itertools.product(self.point_range, self.point_range,
			[self.game_edge], self.game_range, self.set_range_bo3, self.set_range_bo3)

		game_edge2_arg_list = itertools.product(self.point_range, self.point_range,
			self.game_range, [self.game_edge], self.set_range_bo3, self.set_range_bo3)

		set_edge1_arg_list = itertools.product(self.point_range, self.point_range,
			self.game_range, self.game_range, [self.set_edge_bo3], self.set_range_bo3)

		set_edge2_arg_list = itertools.product(self.point_range, self.point_range,
			self.game_range, self.game_range, self.set_range_bo3, [self.set_edge_bo3])

		# for p1, p2, g1, g2, s1, s2 in point_edge1_arg_list:
		# 	inside = imp.overall_importance(p1-1,p2,g1,g2,s1,s2,tiebreak=False,bo3=True)
		# 	outside = imp.overall_importance(p1,p2,g1,g2,s1,s2,tiebreak=False,bo3=True)
		# 	self.assert_almost_equal_or_nans(inside,outside,5)

		# for p1, p2, g1, g2, s1, s2 in point_edge2_arg_list:
		# 	inside = imp.overall_importance(p1,p2-1,g1,g2,s1,s2,tiebreak=False,bo3=True)
		# 	outside = imp.overall_importance(p1,p2,g1,g2,s1,s2,tiebreak=False,bo3=True)
		# 	self.assert_almost_equal_or_nans(inside,outside,5)

		for p1, p2, g1, g2, s1, s2 in game_edge1_arg_list:
			inside = imp.overall_importance(p1,p2,g1-1,g2,s1,s2,tiebreak=False,bo3=True)
			outside = imp.overall_importance(p1,p2,g1,g2,s1,s2,tiebreak=False,bo3=True)
			self.assert_almost_equal_or_nans(inside,outside,5)

		for p1, p2, g1, g2, s1, s2 in game_edge2_arg_list:
			inside = imp.overall_importance(p1,p2,g1,g2-1,s1,s2,tiebreak=False,bo3=True)
			outside = imp.overall_importance(p1,p2,g1,g2,s1,s2,tiebreak=False,bo3=True)
			self.assert_almost_equal_or_nans(inside,outside,5)

		for p1, p2, g1, g2, s1, s2 in set_edge1_arg_list:
			inside = imp.overall_importance(p1,p2,g1,g2,s1-1,s2,tiebreak=False,bo3=True)
			outside = imp.overall_importance(p1,p2,g1,g2,s1,s2,tiebreak=False,bo3=True)
			self.assert_almost_equal_or_nans(inside,outside,5)

		for p1, p2, g1, g2, s1, s2 in set_edge2_arg_list:
			inside = imp.overall_importance(p1,p2,g1,g2,s1,s2-1,tiebreak=False,bo3=True)
			outside = imp.overall_importance(p1,p2,g1,g2,s1,s2,tiebreak=False,bo3=True)
			self.assert_almost_equal_or_nans(inside,outside,5)

	def test_overall_importance_bo3_tb_edge(self):
		''' Test that overall importance works for the edge cases for bo3 and tiebreak. '''

		# point_edge1_arg_list = itertools.product([self.point_edge], self.point_range_tb,
		# 	self.game_range, self.game_range, self.set_range_bo3, self.set_range_bo3)

		# point_edge2_arg_list = itertools.product(self.point_range_tb, [self.point_edge],
		# 	self.game_range, self.game_range, self.set_range_bo3, self.set_range_bo3)

		game_edge1_arg_list = itertools.product(self.point_range_tb, self.point_range_tb,
			[self.game_edge], self.game_range, self.set_range_bo3, self.set_range_bo3)

		game_edge2_arg_list = itertools.product(self.point_range_tb, self.point_range_tb,
			self.game_range, [self.game_edge], self.set_range_bo3, self.set_range_bo3)

		set_edge1_arg_list = itertools.product(self.point_range_tb, self.point_range_tb,
			self.game_range, self.game_range, [self.set_edge_bo3], self.set_range_bo3)

		set_edge2_arg_list = itertools.product(self.point_range_tb, self.point_range_tb,
			self.game_range, self.game_range, self.set_range_bo3, [self.set_edge_bo3])

		# for p1, p2, g1, g2, s1, s2 in point_edge1_arg_list:
		# 	inside = imp.overall_importance(p1-1,p2,g1,g2,s1,s2,tiebreak=True,bo3=True)
		# 	outside = imp.overall_importance(p1,p2,g1,g2,s1,s2,tiebreak=True,bo3=True)
		# 	self.assert_almost_equal_or_nans(inside,outside,5)

		# for p1, p2, g1, g2, s1, s2 in point_edge2_arg_list:
		# 	inside = imp.overall_importance(p1,p2-1,g1,g2,s1,s2,tiebreak=True,bo3=True)
		# 	outside = imp.overall_importance(p1,p2,g1,g2,s1,s2,tiebreak=True,bo3=True)
		# 	self.assert_almost_equal_or_nans(inside,outside,5)

		for p1, p2, g1, g2, s1, s2 in game_edge1_arg_list:
			inside = imp.overall_importance(p1,p2,g1-1,g2,s1,s2,tiebreak=True,bo3=True)
			outside = imp.overall_importance(p1,p2,g1,g2,s1,s2,tiebreak=True,bo3=True)
			self.assert_almost_equal_or_nans(inside,outside,5)

		for p1, p2, g1, g2, s1, s2 in game_edge2_arg_list:
			inside = imp.overall_importance(p1,p2,g1,g2-1,s1,s2,tiebreak=True,bo3=True)
			outside = imp.overall_importance(p1,p2,g1,g2,s1,s2,tiebreak=True,bo3=True)
			self.assert_almost_equal_or_nans(inside,outside,5)

		for p1, p2, g1, g2, s1, s2 in set_edge1_arg_list:
			inside = imp.overall_importance(p1,p2,g1,g2,s1-1,s2,tiebreak=True,bo3=True)
			outside = imp.overall_importance(p1,p2,g1,g2,s1,s2,tiebreak=True,bo3=True)
			self.assert_almost_equal_or_nans(inside,outside,5)

		for p1, p2, g1, g2, s1, s2 in set_edge2_arg_list:
			inside = imp.overall_importance(p1,p2,g1,g2,s1,s2-1,tiebreak=True,bo3=True)
			outside = imp.overall_importance(p1,p2,g1,g2,s1,s2,tiebreak=True,bo3=True)
			self.assert_almost_equal_or_nans(inside,outside,5)


	def test_overall_importance_bo5_non_tb_edge(self):
		''' Test that overall importance works for the edge cases for bo5 and  non-tiebreak. '''

		# point_edge1_arg_list = itertools.product([self.point_edge], self.point_range,
		# 	self.game_range, self.game_range, self.set_range_bo5, self.set_range_bo5)

		# point_edge2_arg_list = itertools.product(self.point_range, [self.point_edge],
		# 	self.game_range, self.game_range, self.set_range_bo5, self.set_range_bo5)

		game_edge1_arg_list = itertools.product(self.point_range, self.point_range,
			[self.game_edge], self.game_range, self.set_range_bo5, self.set_range_bo5)

		game_edge2_arg_list = itertools.product(self.point_range, self.point_range,
			self.game_range, [self.game_edge], self.set_range_bo5, self.set_range_bo5)

		set_edge1_arg_list = itertools.product(self.point_range, self.point_range,
			self.game_range, self.game_range, [self.set_edge_bo5], self.set_range_bo5)

		set_edge2_arg_list = itertools.product(self.point_range, self.point_range,
			self.game_range, self.game_range, self.set_range_bo5, [self.set_edge_bo5])

		# for p1, p2, g1, g2, s1, s2 in point_edge1_arg_list:
		# 	inside = imp.overall_importance(p1-1,p2,g1,g2,s1,s2,tiebreak=False,bo3=False)
		# 	outside = imp.overall_importance(p1,p2,g1,g2,s1,s2,tiebreak=False,bo3=False)
		# 	self.assert_almost_equal_or_nans(inside,outside,5)

		# for p1, p2, g1, g2, s1, s2 in point_edge2_arg_list:
		# 	inside = imp.overall_importance(p1,p2-1,g1,g2,s1,s2,tiebreak=False,bo3=False)
		# 	outside = imp.overall_importance(p1,p2,g1,g2,s1,s2,tiebreak=False,bo3=False)
		# 	self.assert_almost_equal_or_nans(inside,outside,5)

		for p1, p2, g1, g2, s1, s2 in game_edge1_arg_list:
			inside = imp.overall_importance(p1,p2,g1-1,g2,s1,s2,tiebreak=False,bo3=False)
			outside = imp.overall_importance(p1,p2,g1,g2,s1,s2,tiebreak=False,bo3=False)
			self.assert_almost_equal_or_nans(inside,outside,5)

		for p1, p2, g1, g2, s1, s2 in game_edge2_arg_list:
			inside = imp.overall_importance(p1,p2,g1,g2-1,s1,s2,tiebreak=False,bo3=False)
			outside = imp.overall_importance(p1,p2,g1,g2,s1,s2,tiebreak=False,bo3=False)
			self.assert_almost_equal_or_nans(inside,outside,5)

		for p1, p2, g1, g2, s1, s2 in set_edge1_arg_list:
			inside = imp.overall_importance(p1,p2,g1,g2,s1-1,s2,tiebreak=False,bo3=False)
			outside = imp.overall_importance(p1,p2,g1,g2,s1,s2,tiebreak=False,bo3=False)
			self.assert_almost_equal_or_nans(inside,outside,5)

		for p1, p2, g1, g2, s1, s2 in set_edge2_arg_list:
			inside = imp.overall_importance(p1,p2,g1,g2,s1,s2-1,tiebreak=False,bo3=False)
			outside = imp.overall_importance(p1,p2,g1,g2,s1,s2,tiebreak=False,bo3=False)
			self.assert_almost_equal_or_nans(inside,outside,5)

	def test_overall_importance_bo5_tb_edge(self):
		''' Test that overall importance works for the edge cases for bo5 and tiebreak. '''

		# point_edge1_arg_list = itertools.product([self.point_edge], self.point_range_tb,
		# 	self.game_range, self.game_range, self.set_range_bo5, self.set_range_bo5)

		# point_edge2_arg_list = itertools.product(self.point_range_tb, [self.point_edge],
		# 	self.game_range, self.game_range, self.set_range_bo5, self.set_range_bo5)

		game_edge1_arg_list = itertools.product(self.point_range_tb, self.point_range_tb,
			[self.game_edge], self.game_range, self.set_range_bo5, self.set_range_bo5)

		game_edge2_arg_list = itertools.product(self.point_range_tb, self.point_range_tb,
			self.game_range, [self.game_edge], self.set_range_bo5, self.set_range_bo5)

		set_edge1_arg_list = itertools.product(self.point_range_tb, self.point_range_tb,
			self.game_range, self.game_range, [self.set_edge_bo5], self.set_range_bo5)

		set_edge2_arg_list = itertools.product(self.point_range_tb, self.point_range_tb,
			self.game_range, self.game_range, self.set_range_bo5, [self.set_edge_bo5])

		# for p1, p2, g1, g2, s1, s2 in point_edge1_arg_list:
		# 	inside = imp.overall_importance(p1-1,p2,g1,g2,s1,s2,tiebreak=True,bo3=False)
		# 	outside = imp.overall_importance(p1,p2,g1,g2,s1,s2,tiebreak=True,bo3=False)
		# 	self.assert_almost_equal_or_nans(inside,outside,5)

		# for p1, p2, g1, g2, s1, s2 in point_edge2_arg_list:
		# 	inside = imp.overall_importance(p1,p2-1,g1,g2,s1,s2,tiebreak=True,bo3=False)
		# 	outside = imp.overall_importance(p1,p2,g1,g2,s1,s2,tiebreak=True,bo3=False)
		# 	self.assert_almost_equal_or_nans(inside,outside,5)

		for p1, p2, g1, g2, s1, s2 in game_edge1_arg_list:
			inside = imp.overall_importance(p1,p2,g1-1,g2,s1,s2,tiebreak=True,bo3=False)
			outside = imp.overall_importance(p1,p2,g1,g2,s1,s2,tiebreak=True,bo3=False)
			self.assert_almost_equal_or_nans(inside,outside,5)

		for p1, p2, g1, g2, s1, s2 in game_edge2_arg_list:
			inside = imp.overall_importance(p1,p2,g1,g2-1,s1,s2,tiebreak=True,bo3=False)
			outside = imp.overall_importance(p1,p2,g1,g2,s1,s2,tiebreak=True,bo3=False)
			self.assert_almost_equal_or_nans(inside,outside,5)

		for p1, p2, g1, g2, s1, s2 in set_edge1_arg_list:
			inside = imp.overall_importance(p1,p2,g1,g2,s1-1,s2,tiebreak=True,bo3=False)
			outside = imp.overall_importance(p1,p2,g1,g2,s1,s2,tiebreak=True,bo3=False)
			self.assert_almost_equal_or_nans(inside,outside,5)

		for p1, p2, g1, g2, s1, s2 in set_edge2_arg_list:
			inside = imp.overall_importance(p1,p2,g1,g2,s1,s2-1,tiebreak=True,bo3=False)
			outside = imp.overall_importance(p1,p2,g1,g2,s1,s2,tiebreak=True,bo3=False)
			self.assert_almost_equal_or_nans(inside,outside,5)