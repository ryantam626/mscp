import mscp.monte_carlo.monte_carlo as mc
import numpy as np

from unittest import TestCase

class TestMonteCarlo(TestCase):

	def iid_win_prob_extractor(self, server, serve_win_probs):
		''' Function to extract the server winning

		Args:
			server (str) : player name
			serve_win_probs : dictionary of probability of server winning the point

		Returns:
			_ (float) : probability of server winning the point
		'''
		return serve_win_probs[server]

	def iid_scorer(self, score, server):
		''' Function to be consumed by Monte Carlo's play_match function

		Notes:
			# Use global serve_win_probabilities.
			# Not dependent on the score, hence iid 
		'''
		return self.iid_win_prob_extractor(server, self.serve_win_probabilities)

	# ------------------------------------------------------------------------

	@classmethod
	def setup_class(cls):
		# set game parameters:
		cls.p1 = "Roger Federer"
		cls.p2 = "Rafael Nadal"
		cls.bo5 = True
		cls.trials = 1000

	def setUp(self):
		self.games = list()
		self.scores = list()
		self.win_nums = {self.p1: 0, self.p2: 0}

	def test_monte_carlo_deterministic(self):
		''' Test that Monte Carlo simulator runs under deterministic condition. '''
		self.serve_win_probabilities = {self.p1: 1, self.p2: 0}

		for i in range(self.trials):
			first_server = np.random.choice([self.p1, self.p2])
			final_score = mc.play_match(mc.Score(self.p1, self.p2, self.bo5), 
											self.p1, self.p2, self.iid_scorer)
			self.win_nums[final_score.winner] += 1
			self.games.append(final_score.total_games)
			self.scores.append(final_score)

		games = np.array(self.games)
		win_probs = {x: self.win_nums[x] / float(self.trials) for x in self.win_nums}

		pct_won_serve_list = [score.pct_won_serve for score in self.scores]

		p1_pct = [p[self.p1] for p in pct_won_serve_list]
		p2_pct = [p[self.p2] for p in pct_won_serve_list]

		# asserts
		self.assertEqual(win_probs[self.p1],1.0)
		self.assertEqual(win_probs[self.p2],0.0)
		self.assertEqual(np.average(games),6.0)

		self.assertEqual(np.average(p1_pct), self.serve_win_probabilities[self.p1])
		self.assertEqual(np.average(p2_pct), self.serve_win_probabilities[self.p2])

	def test_monte_carlo_random(self):
		''' Test that Monte Carlo simulator runs under probabilistic condition. '''
		self.serve_win_probabilities = {self.p1: 0.6, self.p2: 0.58}

		for i in range(self.trials):
			first_server = np.random.choice([self.p1, self.p2])
			final_score = mc.play_match(mc.Score(self.p1, self.p2, self.bo5), 
											self.p1, self.p2, self.iid_scorer)
			self.win_nums[final_score.winner] += 1
			self.games.append(final_score.total_games)
			self.scores.append(final_score)

		games = np.array(self.games)
		win_probs = {x: self.win_nums[x] / float(self.trials) for x in self.win_nums}

		pct_won_serve_list = [score.pct_won_serve for score in self.scores]

		p1_pct = [p[self.p1] for p in pct_won_serve_list]
		p2_pct = [p[self.p2] for p in pct_won_serve_list]

		# asserts
		self.assertAlmostEqual(np.average(p1_pct), self.serve_win_probabilities[self.p1], 1)
		self.assertAlmostEqual(np.average(p2_pct), self.serve_win_probabilities[self.p2], 1)