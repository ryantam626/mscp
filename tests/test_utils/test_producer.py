from unittest import TestCase

import pandas as pd

from mscp.utils.producer import (InstanceGroup, PassByProducer)

class TestProducer(TestCase):
	''' Tests for producers. '''

	@classmethod
	def setup_class(cls):
		cls.training_instances = [{'player1':'a', 'player2':'b', 'id':1},
								  {'player1':'b', 'player2':'a', 'id':1},
								  {'player1':'a', 'player2':'b', 'id':1},
								  {'player1':'b', 'player2':'c', 'id':2},
								  {'player1':'c', 'player2':'d', 'id':3},
								  {'player1':'b', 'player2':'c', 'id':4},
								  {'player1':'e', 'player2':'c', 'id':5},
								  {'player1':'b', 'player2':'d', 'id':6},
								  {'player1':'f', 'player2':'g', 'id':7},
								  {'player1':'a', 'player2':'b', 'id':8},
								  {'player1':'f', 'player2':'g', 'id':9},
								 ]

		cls.training_instances2 = [{'player1':'a', 'player2':'b', 'id':1},
								   {'player1':'b', 'player2':'a', 'id':1},
								   {'player1':'a', 'player2':'b', 'id':1},
								   {'player1':'b', 'player2':'c', 'id':2},
								   {'player1':'c', 'player2':'d', 'id':3},
								   {'player1':'d', 'player2':'c', 'id':3},
								   {'player1':'b', 'player2':'c', 'id':4},
								   {'player1':'e', 'player2':'c', 'id':5},
								   {'player1':'b', 'player2':'d', 'id':6},
								   {'player1':'f', 'player2':'g', 'id':7},
								   {'player1':'a', 'player2':'b', 'id':8},
								   {'player1':'f', 'player2':'g', 'id':9},
								  ]
		cls.cv_indexes = [1,
						  1,
						  1,
						  1,
						  1,
						  2,
						  1,
						  1,
						  1,
						  1,
						  1,
						  1]

	@staticmethod
	def extractor(row):
		return {'winner':row['player1'], 'loser':row['player2'], 'identifier':row['id']}

	def compare_feature(self, feat_dict, instance):
		self.assertEqual(feat_dict['player1'], instance['winner'])
		self.assertEqual(feat_dict['player2'], instance['loser'])

	def test_no_cv(self):
		''' Test it works with no cross validation index. '''

		df = pd.DataFrame(self.training_instances)
		producer = PassByProducer(df, 'id', self.extractor)

		instance_group = producer.get_next_instance_group()

		for dictionary in self.training_instances:
			instance = instance_group.popleft()
			if not instance:
				instance_group = producer.get_next_instance_group()
				instance = instance_group.popleft()

			self.compare_feature(dictionary, instance)

	def test_cv(self):
		''' Test it works with cross validation index. '''
		df = pd.DataFrame(self.training_instances2)
		producer = PassByProducer(df, 'id', self.extractor, self.cv_indexes, 1)

		instance_group = producer.get_next_instance_group()

		for dictionary, cv_ind in zip(self.training_instances2, self.cv_indexes):
			instance = instance_group.popleft()
			if not instance:
				instance_group = producer.get_next_instance_group()
				instance = instance_group.popleft()
			print(instance_group.context['train'], dictionary, cv_ind)
			self.assertEqual(instance_group.context['train'], cv_ind != 1)
			self.compare_feature(dictionary, instance)