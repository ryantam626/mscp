from unittest import TestCase

from mscp.trueskill.environment import H2HTrueSkillEnvironment

class TestEnvironment(TestCase):
	''' Tests for the Head-to-Head TrueSkill environment.

	Notes:
		# Values for the test is calculated from
		http://boson.research.microsoft.com/trueskill/rankcalculator.aspx
		# remember to set the draw prob to 0%!
	'''

	def test_learn(self):
		environment = H2HTrueSkillEnvironment(initial_mu=25., initial_sigma=25/3.,
											  beta=25/6., dynamic=25/600.)


		player1 = environment.create_trueskill_rating()
		player2 = environment.create_trueskill_rating()

		winner, loser = environment.learn(player1, player2)

		self.assertAlmostEqual(winner.mu, 29.205, 3)
		self.assertAlmostEqual(winner.sigma, 7.195, 3)

		self.assertAlmostEqual(loser.mu, 20.795, 3)
		self.assertAlmostEqual(loser.sigma, 7.195, 3)