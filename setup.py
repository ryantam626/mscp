from setuptools import setup, find_packages

setup(
    name='mscp',
    version='0.1',
    description='Imperial MSc. Individual Project about tennis ',
    packages=find_packages(),
    author='Ryan Tam',
    author_email='ryantam626@hotmail.com', 
)
