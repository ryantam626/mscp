import os
import pandas as pd
import itertools
import numpy as np

from collections import defaultdict

from mscp.utils import aggr_res_group
from mscp.utils.io import save_aggrd_res_group, save_object
from mscp.utils.cv import get_crossval_ind
from mscp.utils.producer import PassByProducer, match_feature_extractor
from mscp.utils.workers import train_trueskill_worker
from mscp.utils.helpers import metric_aggregate_helper
from mscp.trueskill.environment import H2HTrueSkillEnvironment
from mscp.trueskill.storage import BundledStorage

N_FOLDS = 10
N_REPEATS = 10
CSV_DIR = '/home/ryan/workspace/imp/y5/project/mscp/data/atp_main/'
# CSV_PATH = os.path.join(CSV_DIR, 'match_naive_train_min0.csv')
CSV_PATH = os.path.join(CSV_DIR, 'match_naive_train_min20.csv')

# get the dataframe from csv storage
df = pd.DataFrame.from_csv(CSV_PATH)

# assign cross validation indexes
cv_indexes_list = [get_crossval_ind(n=len(df),n_folds=N_FOLDS,seed=s) for s in range(N_REPEATS)]

# broadcast the underlying df and cv_indexes_list
df_bc = sc.broadcast(df)
cv_indexes_list_bc = sc.broadcast(cv_indexes_list)

## make the argument tuple list for spark
cv_index_list = range(N_FOLDS)
cv_rep_index_list = range(N_REPEATS)

# grid 1
# initial_mu_list = [25, 32.5, 40, 47.5]
# initial_sigma_list = [8,11.5,15]
# beta_list = [4,7,10,13]
# dynamic_list = [0.08, 0.18, 0.28]

# grid 1 filtered
# initial_mu_list = [30, 40]
# initial_sigma_list = [8,11.5,15]
# beta_list = [4,7,10,13]
# dynamic_list = [0.08, 0.18, 0.28]

# grid 2
initial_mu_list = [35]
initial_sigma_list = [4.5, 8.0, 11.5, 15.0, 19.5]
beta_list = [3.5, 6.0, 7.5, 9.0, 10.5, 12.0, 13.5]
dynamic_list = [0.1, 0.4, 0.7, 1.0, 1.3]

# [(cv_rep_ind, cv_ind, mu, sigma, beta, dynamic), ...]
hyperparam_with_cv_list = list(itertools.product(cv_rep_index_list, cv_index_list, initial_mu_list,initial_sigma_list,beta_list,dynamic_list))
# [(pair_ind, (cv_rep_ind, cv_ind, mu, sigma, beta, dynamic)), ...]
indexed_param_grid = list(zip(range(len(hyperparam_with_cv_list)), hyperparam_with_cv_list))
# # TODO: remove this
# indexed_param_grid = indexed_param_grid[1:30]
# parallelize it for spark
par_indexed_param_grid = sc.parallelize(indexed_param_grid, len(indexed_param_grid))

# CAV: must define it here for the broadcasted value
def train_and_eval(tup):
	# unpack the args
	pair_ind, (cv_rep_ind, cv_ind, *hyperparam_list) = tup
	# grab the bc values
	df_local = df_bc.value
	cv_indexes_list_local = cv_indexes_list_bc.value
	# init the env/cal/prod for TS
	environment = H2HTrueSkillEnvironment(*hyperparam_list)
	producer = PassByProducer(df_local, 'match_identifier',
		match_feature_extractor, cv_indexes_list_local[cv_rep_ind], cv_ind)
	# init the storages for TS
	bstorage = BundledStorage(environment)
	# actually training and eval TS
	train_trueskill_worker(environment, bstorage, producer)
	# aggr metrics
	aggregated_metric = metric_aggregate_helper(bstorage, 50)
	return (pair_ind, cv_rep_ind, cv_ind, hyperparam_list, aggregated_metric)

res_list = par_indexed_param_grid.map(train_and_eval).collect()

res_group = defaultdict(list)

for (pair_ind, cv_rep_ind, cv_ind, hyperparam_list, aggregated_metric) in res_list:
	element = (pair_ind, cv_rep_ind, cv_ind, aggregated_metric)
	res_group[tuple(hyperparam_list)].append(element)

aggrd_res_group = aggr_res_group(res_group)

# save_aggrd_res_group(aggrd_res_group)

# save_object(res_list)