import pickle
import os

BASE_DIR = './data/saved_results'

list_of_files = os.listdir(BASE_DIR)

filtered_list_of_files = list(filter(lambda s:s.endswith('test.pkl') and (s.startswith('point') or s.startswith('match')) and not s.startswith('point_adjusted'), list_of_files))

d = dict()

for f in filtered_list_of_files:
	path = os.path.join(BASE_DIR, f)
	with open(path,'rb') as file:
		r = pickle.load(file)

	split_name = f.split('_')
	level = split_name[0].capitalize()
	scheme = split_name[2].capitalize()

	brier = r[0][0]
	error_rate = r[2][0] / r[1][0]

	d[(level, scheme)] = (brier, error_rate)

import pandas

def gen():
	for k,v in sorted(d.items()):
		yield k[0], k[1], v[0], v[1]

pandas.DataFrame(gen(), columns=['level', 'scheme', 'brier_score', 'error_rate']).to_csv(os.path.join(BASE_DIR, 'naive_result.csv'), index=False)