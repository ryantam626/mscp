# model config
MU = 35
DYNAMIC = 0.7
BETA = 7.5
SIGMA = 19.5

import os
import pandas as pd
import pickle

from mscp.trueskill.environment import H2HTrueSkillEnvironment
from mscp.trueskill.storage import BundledStorage
from mscp.utils.producer import PassByProducer, match_feature_extractor
from mscp.utils.workers import train_trueskill_worker
from mscp.utils.helpers import metric_aggregate_helper

# load the train and test file
CSV_DIR = '/home/ryan/workspace/imp/y5/project/mscp/data/atp_main/'
TRAIN_CSV_PATH = os.path.join(CSV_DIR, 'match_naive_train_min20.csv')
df_train = pd.DataFrame.from_csv(TRAIN_CSV_PATH)
TEST_CSV_PATH = os.path.join(CSV_DIR, 'match_test.csv')
df_test = pd.DataFrame.from_csv(TEST_CSV_PATH)


# train
environment = H2HTrueSkillEnvironment(MU, SIGMA, BETA, DYNAMIC)
train_producer = PassByProducer(df_train, 'match_identifier', match_feature_extractor)
storage = BundledStorage(environment)
train_trueskill_worker(environment, storage, train_producer)
storage.convert_skill_for_pickle()
storage.reset_metric()

with open('./data/saved_models/match_naive_error.pkl', 'wb') as f:
	pickle.dump((environment, storage), f)

storage.convert_to_cython_skill()

# test
test_producer = PassByProducer(df_test, 'match_identifier', match_feature_extractor)
train_trueskill_worker(environment, storage, test_producer, skip=True)

test_res = metric_aggregate_helper(storage, 0)
with open('./data/saved_results/match_naive_error_match_test.pkl', 'wb') as f:
	pickle.dump(test_res, f)