# model config
MU = 35
DYNAMIC = 0.02
BETA = 8
SIGMA = 8
ADJ_MU = 0
ADJ_SIGMA = 2
ADJ_DYNAMIC = 0.02

import os
import pandas as pd
import pickle

from mscp.trueskill.environment import AdjustedH2HTrueSkillEnvironment
from mscp.trueskill.storage import BundledStorage
from mscp.utils.producer import PassByProducer, point_feature_extractor_g
from mscp.utils.workers import train_adjusted_trueskill_worker
from mscp.utils.helpers import metric_aggregate_helper

# load the train and test file
CSV_DIR = '/home/ryan/workspace/imp/y5/project/mscp/data/atp_main/'
TRAIN_CSV_PATH = os.path.join(CSV_DIR, 'point_naive_train_min20.csv')
df_train = pd.DataFrame.from_csv(TRAIN_CSV_PATH)
TEST_CSV_PATH = os.path.join(CSV_DIR, 'point_test.csv')
df_test = pd.DataFrame.from_csv(TEST_CSV_PATH)

# train
environment = AdjustedH2HTrueSkillEnvironment(MU, SIGMA, BETA, DYNAMIC, ADJ_MU, ADJ_SIGMA, ADJ_DYNAMIC, None, 1)
train_producer = PassByProducer(df_train, 'match_identifier', point_feature_extractor_g)
storage = BundledStorage(environment)
adj_storage = BundledStorage(make_skill=environment.create_trueskill_adjustment)
train_adjusted_trueskill_worker(environment, storage, adj_storage, train_producer)
# NOTE: dont really need to store this metric anyway
storage.reset_metric()
storage.convert_skill_for_pickle()
adj_storage.reset_metric()
adj_storage.convert_skill_for_pickle()

with open('./data/saved_models/point_adjusted_g_error.pkl', 'wb') as f:
	pickle.dump((environment, storage, adj_storage), f)

storage.convert_to_cython_skill()
adj_storage.convert_to_cython_skill()

# test
test_producer = PassByProducer(df_test, 'match_identifier', point_feature_extractor_g)
train_adjusted_trueskill_worker(environment, storage, adj_storage, test_producer, skip=True)

test_res = metric_aggregate_helper(storage, 0)
with open('./data/saved_results/point_adjusted_g_error_point_test.pkl', 'wb') as f:
	pickle.dump(test_res, f)