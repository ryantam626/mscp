import pickle
import os

BASE_DIR = './data/saved_results'

list_of_files = os.listdir(BASE_DIR)

filtered_list_of_files = list(filter(lambda s:s.endswith('test.pkl') and s.startswith('point_adjusted'), list_of_files))

d = dict()

for f in filtered_list_of_files:
	path = os.path.join(BASE_DIR, f)
	with open(path,'rb') as file:
		r = pickle.load(file)

	split_name = f.split('_')
	ident = split_name[2].upper()
	scheme = split_name[3].capitalize()

	brier = r[0][0]
	error_rate = r[2][0] / r[1][0]

	d[(ident, scheme)] = (brier, error_rate)

import pandas

def gen():
	for k,v in sorted(d.items()):
		yield k[0], k[1], v[0], v[1]

pandas.DataFrame(gen(), columns=['state_extractor', 'scheme', 'brier_score', 'error_rate']).to_csv(os.path.join(BASE_DIR, 'point_adjusted_results.csv'), index=False)