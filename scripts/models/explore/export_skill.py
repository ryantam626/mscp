import pickle
import os
import pandas as pd

from mscp.utils.producer import PassByProducer, point_feature_extractor_b
from mscp.utils.workers import train_adjusted_trueskill_worker

BASE_DIR = './data/explore'

# load the train and test file
CSV_DIR = '/home/ryan/workspace/imp/y5/project/mscp/data/atp_main/'
TEST_CSV_PATH = os.path.join(CSV_DIR, 'point_test.csv')
df_test = pd.DataFrame.from_csv(TEST_CSV_PATH)

with open('./data/saved_models/point_adjusted_b_brier.pkl', 'rb') as f:
	environment, storage, adj_storage = pickle.load(f)

storage.convert_to_cython_skill()
adj_storage.convert_to_cython_skill()

# test
test_producer = PassByProducer(df_test, 'match_identifier', point_feature_extractor_b)
train_adjusted_trueskill_worker(environment, storage, adj_storage, test_producer, skip=True)

storage.convert_skill_for_pickle()
adj_storage.convert_skill_for_pickle()

def gen(s):
	for k, v in s.python_skill.items():
		yield k, v[0], v[1]

def adj_gen(s):
	for k, v in s.python_skill.items():
		yield k[0], k[1], v[0], v[1]

pd.DataFrame(gen(storage), columns=['player', 'skill_mu', 'skill_sig']).to_csv(os.path.join(BASE_DIR, 'skill.csv'), index=False)
pd.DataFrame(adj_gen(adj_storage), columns=['state', 'player', 'skill_mu', 'skill_sig']).to_csv(os.path.join(BASE_DIR, 'adj_skill.csv'), index=False)

