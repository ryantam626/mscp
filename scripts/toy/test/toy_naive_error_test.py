# model config
MU = 35
DYNAMIC = 0.02
BETA = 21
SIGMA = 15

import os
import pandas as pd
import pickle

from mscp.trueskill.environment import H2HTrueSkillEnvironment
from mscp.trueskill.storage import BundledStorage
from mscp.utils.producer import PassByProducer
from mscp.utils.workers import train_trueskill_worker
from mscp.utils.helpers import metric_aggregate_helper

# load the train and test file
CSV_DIR = '/home/ryan/workspace/imp/y5/project/mscp/data/toy/'
TRAIN_CSV_PATH = os.path.join(CSV_DIR, 'train.csv')
df_train = pd.DataFrame.from_csv(TRAIN_CSV_PATH)
TEST_CSV_PATH = os.path.join(CSV_DIR, 'test.csv')
df_test = pd.DataFrame.from_csv(TEST_CSV_PATH)

def naive_feature_extractor(row):
	winner = row['winner']
	loser = row['loser']
	identifier = row['match_id']
	return {'winner':winner, 'loser':loser, 'identifier':identifier}

# train
environment = H2HTrueSkillEnvironment(MU, SIGMA, BETA, DYNAMIC)
train_producer = PassByProducer(df_train, 'match_id', naive_feature_extractor)
storage = BundledStorage(environment)
train_trueskill_worker(environment, storage, train_producer)
storage.reset_metric()
storage.convert_skill_for_pickle()

with open('./data/saved_models/toy_naive_error.pkl', 'wb') as f:
	pickle.dump((environment, storage), f)

storage.convert_to_cython_skill()
# test
test_producer = PassByProducer(df_test, 'match_id', naive_feature_extractor)
storage.reset_metric()
train_trueskill_worker(environment, storage, test_producer, skip=True)

test_res = metric_aggregate_helper(storage, 0)
with open('./data/saved_results/toy_naive_error_test.pkl', 'wb') as f:
	pickle.dump(test_res, f)