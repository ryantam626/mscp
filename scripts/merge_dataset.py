import os
import pandas as pd
import numpy as np

archive_dir = '/home/ryan/workspace/imp/y5/project/mscp/data/tennis_pointbypoint/pbp_matches_atp_main_archive.csv'
current_dir = '/home/ryan/workspace/imp/y5/project/mscp/data/tennis_pointbypoint/pbp_matches_atp_main_current.csv'


archive_df = pd.DataFrame.from_csv(archive_dir)
current_df = pd.DataFrame.from_csv(current_dir)

indexes = np.array_split(np.arange(len(current_df)),2)
current_first_half = current_df.iloc[indexes[0],:]

naive_train = pd.concat([archive_df, current_first_half])
naive_train.to_csv('/home/ryan/workspace/imp/y5/project/mscp/data/custom_source/atp_main_naive_train.csv')
