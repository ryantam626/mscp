import os
import pandas as pd
import numpy as np
import itertools

from mscp.importer import SackmannImporter, ParallelSackmannImporter
from mscp.features.feature_extractors import ClusterFeatureExtractor
from mscp.cluster_config import *

CSV_DIR = '/home/ryan/workspace/imp/y5/project/mscp/data/custom_source/'
csv_name_list = ['atp_main_cluster_train_error.csv']
csv_abspath_list = [os.path.join(CSV_DIR, n) for n in csv_name_list]

sackmann_importer = SackmannImporter(csv_abspath_list,
	feature_extractor=ClusterFeatureExtractor(), min_played=0, include_base_prob=True)

parallelised_importer = ParallelSackmannImporter(sackmann_importer, pool_size=6)

df = parallelised_importer.extract()

# get the list of unique players
player_list = df.server.unique()

def increment_df(df, state, name):
	if pd.np.isnan(df[state][name]):
		df[state][name] = 1
	else:
		df[state][name] += 1

# init the df for storing intermediate values
won_df = pd.DataFrame(index=player_list, columns=all_states)
total_df = pd.DataFrame(index=player_list, columns=all_states)

for ind, row in df.iterrows():
	# general
	is_bp = row['is_bp']
	is_pt_before_bp = row['is_pt_before_bp']
	is_tiebreak = row['is_tiebreak']

	# server
	server_name = row['server']
	server_set_up = row['server_set_up']
	server_set_down = row['server_set_down']
	server_win_prob_bin = bin_win_prob(row['server_win_prob'])
	server_state = (1, is_bp, is_pt_before_bp, is_tiebreak, server_set_up,
		server_set_down, server_win_prob_bin)

	# returner
	returner_name = row['returner']
	returner_set_up = row['returner_set_up']
	returner_set_down = row['returner_set_down']
	returner_win_prob_bin = bin_win_prob(row['returner_win_prob'])
	returner_state = (0, is_bp, is_pt_before_bp, is_tiebreak, returner_set_up,
		returner_set_down, returner_win_prob_bin)

	increment_df(total_df, server_state, server_name)
	increment_df(total_df, returner_state, returner_name)

	if row['server_won']:
		increment_df(won_df, server_state, server_name)
	else:
		increment_df(won_df, returner_state, returner_name)

won_percentage_df = won_df/total_df

for col in won_percentage_df.columns:
	impute_val = (col[-1]+0.5)*BINS/100
	won_percentage_df[col].fillna(impute_val, inplace=True)

from sklearn.cluster import MeanShift, KMeans, AffinityPropagation, Birch
# ms_model = MeanShift()
# ms_cluster_membership = ms_model.fit_predict(won_percentage_df.as_matrix())
# ms_cluster_membership_df = pd.DataFrame(ms_cluster_membership, index=won_percentage_df.index, columns=['cluster'])
# ms_cluster_membership_df.to_csv('/home/ryan/workspace/imp/y5/project/mscp/data/clustering/ms_error.csv')

km_model = KMeans(3,random_state=8888)
km_cluster_membership = km_model.fit_predict(won_percentage_df.as_matrix())
km_cluster_membership_df = pd.DataFrame(km_cluster_membership, index=won_percentage_df.index, columns=['cluster'])
km_cluster_membership_df.to_csv('/home/ryan/workspace/imp/y5/project/mscp/data/clustering/km_error.csv')

ap_model = AffinityPropagation()
ap_cluster_membership = ap_model.fit_predict(won_percentage_df.as_matrix())
ap_cluster_membership_df = pd.DataFrame(ap_cluster_membership, index=won_percentage_df.index, columns=['cluster'])
ap_cluster_membership_df.to_csv('/home/ryan/workspace/imp/y5/project/mscp/data/clustering/ap_error.csv')

bi_model = Birch()
bi_cluster_membership = bi_model.fit_predict(won_percentage_df.as_matrix())
bi_cluster_membership_df = pd.DataFrame(bi_cluster_membership, index=won_percentage_df.index, columns=['cluster'])
bi_cluster_membership_df.to_csv('/home/ryan/workspace/imp/y5/project/mscp/data/clustering/bi_error.csv')


