from mscp.importer import SackmannImporter, ParallelSackmannImporter
from mscp.utils.helpers import train_helper

import os
import pandas as pd
import pickle


# import the csv to be annotated, borrow the utils in sackmann_importer
CSV_DIR = '/home/ryan/workspace/imp/y5/project/mscp/data/custom_source/'
csv_name_list = ['atp_main_naive_train.csv']
csv_abspath_list = [os.path.join(CSV_DIR, n) for n in csv_name_list]
sackmann_importer = SackmannImporter(csv_abspath_list, min_played=20)

df = sackmann_importer.df

# load the model's pkl files 
saved_models_path = '/home/ryan/workspace/imp/y5/project/mscp/data/saved_models/'

with open(os.path.join(saved_models_path, 'match_naive_brier.pkl'),'rb') as f:
	env_brier, stor_brier = pickle.load(f)

with open(os.path.join(saved_models_path, 'match_naive_error.pkl'),'rb') as f:
	env_error, stor_error = pickle.load(f)


def df_gen(env, stor):
	stor.reset_all()

	for ind, row in df.iterrows():
		(winner_name, loser_name) = ((row['server1'], row['server2']) if
							   row['winner'] == 1 else
							   (row['server2'], row['server1']))

		instance ={}
		instance['winner'] = winner_name
		instance['loser'] = loser_name

		winner, loser = stor.from_feature_dict(instance)

		# first eval
		winner_win_prob = env.evaluate(winner, loser)
		loser_win_prob = 1 - winner_win_prob

		updated_winner, updated_loser = env.learn(winner, loser)
		stor.update(instance['winner'], updated_winner, instance['loser'], updated_loser)

		row['winner_win_prob'] = winner_win_prob
		row['loser_win_prob'] = loser_win_prob

		if stor.train_counter[winner_name] > 20:
			if stor.train_counter[loser_name] > 20:
				yield row

		continue

df_brier = pd.DataFrame(df_gen(env_brier, stor_brier))
df_error = pd.DataFrame(df_gen(env_error, stor_error))

CUSTOM_SOURCE_DIR = '/home/ryan/workspace/imp/y5/project/mscp/data/custom_source/'

df_brier.to_csv(os.path.join(CUSTOM_SOURCE_DIR, 'atp_main_cluster_train_brier.csv'))
df_error.to_csv(os.path.join(CUSTOM_SOURCE_DIR, 'atp_main_cluster_train_error.csv'))