import scipy.stats

t = -0.573137944387
eps = 0

x = t - eps
denom = scipy.stats.norm.cdf(x)
numer = scipy.stats.norm.pdf(x)

assert(denom == 0.28327564867540983)
assert(numer == 0.33851662135959948)