# CV config
N_FOLDS = 10
N_REPEATS = 1

import os
import pandas as pd
import itertools

from functools import partial
from collections import defaultdict

from mscp.trueskill.environment import AdjustedH2HTrueSkillEnvironment
from mscp.trueskill.storage import BundledStorage
from mscp.utils import aggr_res_group
from mscp.utils.cv import get_crossval_ind
from mscp.utils.io import save_aggrd_res_group
from mscp.utils.producer import PassByProducer, point_feature_extractor_d_cluster
from mscp.utils.workers import train_adjusted_trueskill_worker
from mscp.utils.helpers import metric_aggregate_helper

# define path
CSV_DIR = '/home/ryan/workspace/imp/y5/project/mscp/data/atp_main/'
TRAIN_CSV_PATH = os.path.join(CSV_DIR, 'point_naive_train_min20.csv')

# load csv
df = pd.DataFrame.from_csv(TRAIN_CSV_PATH)
# define cv ind var
cv_indexes_list = [get_crossval_ind(n=len(df),n_folds=N_FOLDS,seed=s) for s in range(N_REPEATS)]

# broadcast the underlying df and cv_indexes_list
df_bc = sc.broadcast(df)
cv_indexes_list_bc = sc.broadcast(cv_indexes_list)

## make the argument tuple list for spark
cv_index_list = range(N_FOLDS)
cv_rep_index_list = range(N_REPEATS)

# grid 1
initial_mu_list = [35]
initial_sigma_list = [8]
beta_list = [6, 8, 10]
dynamic_list = [0.02]
initial_adj_mu_list = [0]
initial_adj_sigma_list = [2]
adj_dyanmic_list = [0.02]
eps_list = [None]
eta_list = [1]

hyperparam_with_cv_list = list(itertools.product(
	cv_rep_index_list,
	cv_index_list,
	initial_mu_list,
	initial_sigma_list,
	beta_list,
	dynamic_list,
	initial_adj_mu_list,
	initial_adj_sigma_list,
	adj_dyanmic_list,
	eps_list,
	eta_list))

indexed_param_grid = list(zip(range(len(hyperparam_with_cv_list)), hyperparam_with_cv_list))
# parallelize it for spark
par_indexed_param_grid = sc.parallelize(indexed_param_grid, len(indexed_param_grid))

CLUSTERING_DIR = './data/clustering'

for n in filter(lambda x:x.endswith('.csv'), os.listdir(CLUSTERING_DIR)):
	file_name = os.path.join(CLUSTERING_DIR, n)
	cluster_membership = pd.DataFrame.from_csv(file_name)

	feat_ext = partial(point_feature_extractor_d_cluster, cluster_membership_df = cluster_membership)

	# CAV: must define it here for the broadcasted value
	def adjusted(tup):
		# unpack the args
		pair_ind, (cv_rep_ind, cv_ind, *hyperparam_list) = tup
		# grab the bc values
		df_local = df_bc.value
		cv_indexes_list_local = cv_indexes_list_bc.value
		# init the env/cal/prod for TS
		environment = AdjustedH2HTrueSkillEnvironment(*hyperparam_list)
		producer = PassByProducer(df_local, 'match_identifier',
			feat_ext, cv_indexes_list_local[cv_rep_ind], cv_ind)
		# init the storages for TS
		skill_storage = BundledStorage(environment)
		adj_storage = BundledStorage(make_skill=environment.create_trueskill_adjustment)
		# actually training and eval TS
		train_adjusted_trueskill_worker(environment, skill_storage, adj_storage, producer)
		# aggr metrics
		aggregated_metric = metric_aggregate_helper(skill_storage, 50)
		adj_storage.convert_skill_for_pickle()
		skill_storage.convert_skill_for_pickle()
		return (pair_ind, cv_rep_ind, cv_ind, hyperparam_list, aggregated_metric)

	res_list = par_indexed_param_grid.map(adjusted).collect()

	res_group = defaultdict(list)

	for (pair_ind, cv_rep_ind, cv_ind, hyperparam_list, aggregated_metric) in res_list:
		element = (pair_ind, cv_rep_ind, cv_ind, aggregated_metric)
		res_group[tuple(hyperparam_list)].append(element)

	aggrd_res_group = aggr_res_group(res_group)

	def gen():
		for k, v in aggrd_res_group.items():
			(mu, sigma, beta, dynamics, adj_mu, adj_sigma, adj_dynamics, eps, eta) = k
			brier_dict, entry_dict, wrong_dict = v
			for vk in brier_dict.keys():
				yield mu, sigma, beta, dynamics, adj_mu, adj_sigma, adj_dynamics, eps, eta, vk ,brier_dict[vk], entry_dict[vk], wrong_dict[vk]


	res_dir = './data/saved_results/clustered_point_adjusted_d_grid_{}_v1.csv'.format(n.split('.csv')[0])
	pd.DataFrame(gen(), columns=['mu', 'sigma', 'beta', 'dynamics', 'adj_mu', 'adj_sigma', 'adj_dynamics', 'eps', 'eta', 'burin',
		'brier_score', 'total_num_entry', 'total_num_wrong_entry']).to_csv(res_dir)