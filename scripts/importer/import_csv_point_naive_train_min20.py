from mscp.importer import SackmannImporter, ParallelSackmannImporter
from mscp.features.feature_extractors import ClusterFeatureExtractor
import os
import pandas as pd


CSV_DIR = '/home/ryan/workspace/imp/y5/project/mscp/data/custom_source/'
csv_name_list = ['atp_main_naive_train.csv']
csv_abspath_list = [os.path.join(CSV_DIR, n) for n in csv_name_list]

sackmann_importer = SackmannImporter(csv_abspath_list, min_played=20, feature_extractor=ClusterFeatureExtractor())
parallelised_importer = ParallelSackmannImporter(sackmann_importer, pool_size=6)

df = parallelised_importer.extract()
df.to_csv('/home/ryan/workspace/imp/y5/project/mscp/data/atp_main/point_naive_train_min20.csv')