from mscp.importer import SimpleSackmannImporter, ParallelSackmannImporter

import os
import pandas as pd


CSV_DIR = '/home/ryan/workspace/imp/y5/project/mscp/data/tennis_pointbypoint/'
csv_name_list = ['pbp_matches_atp_main_current.csv']
csv_abspath_list = [os.path.join(CSV_DIR, n) for n in csv_name_list]

sackmann_importer = SimpleSackmannImporter(csv_abspath_list, section=(2,2))
parallelised_importer = ParallelSackmannImporter(sackmann_importer, pool_size=6)

df = parallelised_importer.extract()
df.to_csv('/home/ryan/workspace/imp/y5/project/mscp/data/atp_main/match_test.csv')